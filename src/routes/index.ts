import {
  HomeScreen,
  LibraryScreen,
  ProfileScreen,
  ReaderScreen,
  StoreScreen,
} from 'src/screens';

export interface IMenuItem {
  icon: string;
  id: string;
  link: string;
  title: string;
  component: React.ComponentType<any>;
}

const routes: IMenuItem[] = [
  {
    component: HomeScreen,
    icon: 'Home',
    id: 'home',
    link: '/',
    title: 'Home',
  },
  {
    component: StoreScreen,
    icon: 'Store',
    id: 'store',
    link: '/store',
    title: 'Store',
  },
  {
    component: ReaderScreen,
    icon: 'EyeOpen',
    id: 'reader',
    link: '/reader',
    title: 'Reader',
  },
  {
    component: LibraryScreen,
    icon: 'Label',
    id: 'Library',
    link: '/library',
    title: 'Library',
  },
  {
    component: ProfileScreen,
    icon: 'Profile',
    id: 'profile',
    link: '/profile',
    title: 'Profile',
  },
];

export default routes;
