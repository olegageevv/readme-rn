import { routerMiddleware } from 'connected-react-router';
import { createMemoryHistory } from 'history';
import { applyMiddleware, createStore } from 'redux';
import { persistCombineReducers, persistStore } from 'redux-persist';

import storage from 'redux-persist/es/storage';
import createSagaMiddleware from 'redux-saga';

import IRootState from 'src/models';
import rootReducer from 'src/reducers';
import rootSaga from 'src/sagas';
import { composeEnhancers } from './utils';

const persistConfig = {
  storage,
  blacklist: [],
  key: 'root',
};

const history = createMemoryHistory();

const sagaMiddleware = createSagaMiddleware();
const middleware = [
  routerMiddleware(history),
  sagaMiddleware,
];

const configureStore = (initialState?: IRootState) => {
  const store = createStore<IRootState, any, any, any>(
    persistCombineReducers(persistConfig, rootReducer(history)),
    initialState!,
    composeEnhancers(applyMiddleware(...middleware)),
  );
  sagaMiddleware.run(rootSaga);

  const persistor = persistStore(
    store,
    {},
    () => store.getState(),
  );

  return { persistor, history, store };
};

export default configureStore;
