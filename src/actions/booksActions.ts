import { AnyAction } from 'redux';

/*
 * Define action name constants here
 */
export enum ActionTypes {
  BOOK_OPEN = 'books/BOOK_OPEN',
}

/*
 * Define return types of actions
 */

export interface IActionBookOpen extends AnyAction {
  type: ActionTypes.BOOK_OPEN;
  payload: { id: string };
}

/*
 * Define actions creators
 */

/**
 * Open book screen by book id
 *
 * @export
 * @param {string} id
 * @returns {IActionBookOpen}
 */
export function bookOpen (id: string): IActionBookOpen {
  return {
    payload: { id },
    type: ActionTypes.BOOK_OPEN,
  };
}

/*
 * Define the Action type
 * It can be one of the types defining in our action/todos file
 * It will be useful to tell typescript about our types in our reducer
 */
export type Action = IActionBookOpen;
