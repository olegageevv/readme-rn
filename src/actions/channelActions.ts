import { AnyAction } from 'redux';

import { IMessage, TChannel } from 'src/models/IChannelState';

/*
 * Define action name constants here
 */
export enum ActionTypes {
  MESSAGE_POST = 'channel/MESSAGE_POST',
}

/*
 * Define return types of actions
 */

export interface IActionMessagePost extends AnyAction {
  type: ActionTypes.MESSAGE_POST;
  payload: { message: IMessage, channelId: string, type: TChannel };
}

/*
 * Define actions creators
 */

/**
 * Add message
 *
 * @export
 * @param {IMessage} message
 * @param {string} channelId
 * @param {TChannel} type
 * @returns {IActionMessagePost}
 */
export function messagePost (
  message: IMessage,
  channelId: string,
  type: TChannel,
): IActionMessagePost {
  return {
    payload: { message, channelId, type },
    type: ActionTypes.MESSAGE_POST,
  };
}

/*
 * Define the Action type
 * It can be one of the types defining in our action/todos file
 * It will be useful to tell typescript about our types in our reducer
 */
export type Action = IActionMessagePost;
