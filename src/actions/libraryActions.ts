import { AnyAction } from 'redux';

/*
 * Define action name constants here
 */
export enum ActionTypes {
  COLLECTION_OPEN = 'library/COLLECTION_OPEN',
}

/*
 * Define return types of actions
 */

export interface IActionCollectionOpen extends AnyAction {
  type: ActionTypes.COLLECTION_OPEN;
  payload: { id: string };
}

/*
 * Define actions creators
 */

/**
 * Open collection
 *
 * @export
 * @param {string} id
 * @returns {IActionCollectionOpen}
 */
export function collectionOpen (id: string): IActionCollectionOpen {
  return {
    payload: { id },
    type: ActionTypes.COLLECTION_OPEN,
  };
}

/*
 * Define the Action type
 * It can be one of the types defining in our action/todos file
 * It will be useful to tell typescript about our types in our reducer
 */
export type Action = IActionCollectionOpen;
