import { AnyAction } from 'redux';

/*
 * Define action name constants here
 */
export enum ActionTypes {
  STORE_CATEGORY_OPEN = 'store/STORE_CATEGORY_OPEN',
}

/*
 * Define return types of actions
 */

export interface IActionStoreCategoryOpen extends AnyAction {
  type: ActionTypes.STORE_CATEGORY_OPEN;
  payload: { id: string };
}

/*
 * Define actions creators
 */

/**
 * Open category
 *
 * @export
 * @param {string} id
 * @returns {IActionStoreCategoryOpen}
 */
export function categoryOpen (id: string): IActionStoreCategoryOpen {
  return {
    payload: { id },
    type: ActionTypes.STORE_CATEGORY_OPEN,
  };
}

/*
 * Define the Action type
 * It can be one of the types defining in our action/todos file
 * It will be useful to tell typescript about our types in our reducer
 */
export type Action = IActionStoreCategoryOpen;
