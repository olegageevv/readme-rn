import { AnyAction } from 'redux';

/*
 * Define action name constants here
 */
export enum ActionTypes {
  SET_LOADING = 'ui/SET_LOADING',
  DISABLE_INTRO = 'ui/DISABLE_INTRO',
}

/*
 * Define return types of actions
 */

export interface IActionSetLoading extends AnyAction {
  type: ActionTypes.SET_LOADING;
  payload: { loading: boolean };
}

export interface IActionDisableIntro extends AnyAction {
  type: ActionTypes.DISABLE_INTRO;
}

/*
 * Define actions creators
 */

/**
 * Set global loading status of app
 *
 * @export
 * @param {boolean} loading
 * @returns {IActionSetLoading}
 */
export function setLoading (loading: boolean): IActionSetLoading {
  return {
    payload: {
      loading,
    },
    type: ActionTypes.SET_LOADING,
  };
}

/**
 * Disable intro slides
 *
 * @export
 * @returns {IActionDisableIntro}
 */
export function disableIntro (): IActionDisableIntro {
  return {
    type: ActionTypes.DISABLE_INTRO,
  };
}

/*
 * Define the Action type
 * It can be one of the types defining in our action/todos file
 * It will be useful to tell typescript about our types in our reducer
 */
export type Action = IActionSetLoading
  | IActionDisableIntro
  ;
