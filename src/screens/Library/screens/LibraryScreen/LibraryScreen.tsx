import { push } from 'connected-react-router';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Books, Header, HorizontalScroll, IMenuItem, List, Menu } from 'src/components';
import IRootState, { IBooksState } from 'src/models';

import styles from './LibraryScreen.styles';

interface IProps {
  books: IBooksState;
  bookOpen: (bookId: string) => void;
  collectionOpen: (collectionId: string) => void;
}

class BookScreen extends React.Component<IProps> {
  menuItems: IMenuItem[] = [
    {
      icon: 'Plus',
      id: '1',
      onPress: () => this.props.collectionOpen('1'),
      title: 'Added',
    },
    {
      icon: 'Search',
      id: '2',
      onPress: () => this.props.collectionOpen('1'),
      title: 'Quotes',
    },
    {
      icon: 'Check',
      id: '3',
      onPress: () => this.props.collectionOpen('1'),
      title: 'Finished',
    },
    {
      icon: 'Book',
      id: '4',
      onPress: () => this.props.collectionOpen('2'),
      title: 'All Books',
    },
  ];

  render () {
    const { books, bookOpen } = this.props;
    const lastBooks = Object.keys(books.items).map(key => books.items[key]);
    return (
      <View style={styles.container}>
        <Header
          title="My Library"
          rightItems={[{ icon: 'Plus', action: () => undefined }]}
        />
        <ScrollView>
          <HorizontalScroll>
            <Books data={lastBooks} onPress={bookOpen} />
          </HorizontalScroll>
          <Menu items={this.menuItems} />
          <List type="library" title="Bookshelves" />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  books: state.books,
});

const mapDispatchToProps = {
  bookOpen: (bookId: string) => push(`/book/${bookId}`),
  collectionOpen: (collectionId: string) => push(`/library/${collectionId}`),
};

export default withRouter(connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(BookScreen));
