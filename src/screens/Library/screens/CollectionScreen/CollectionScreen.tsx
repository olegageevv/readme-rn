import { goBack } from 'connected-react-router';
import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { match as IMatch } from 'react-router-native';

import { Header, List } from 'src/components';
import IRootState, { ILibraryState } from 'src/models';

import styles from './CollectionScreen.styles';

interface IParams {
  collectionId: string;
}

export interface IProps {
  library: ILibraryState;
  match: IMatch<IParams>;
  goToBack: () => void;
}

class CollectionScreen extends React.Component<IProps> {
  onBackClick = () => {
    this.props.goToBack();
  }

  render () {
    const {
      library,
      match,
    } = this.props;
    const { collectionId } = match.params;
    const collection = library.items[`id-${collectionId}`];
    return (
      <View style={styles.container}>
        <Header
          title={collection.title}
          leftItems={[
            { icon: 'Back', action: this.onBackClick },
          ]}
        />
        <List
          type="books"
          ids={collection.books}
          numColumns={2}
        />
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  library: state.library,
});

const mapDispatchToProps = {
  goToBack: () => goBack(),
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(CollectionScreen);
