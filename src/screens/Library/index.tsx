import React from 'react';
import {
  match as IMatch,
  Route,
  Switch,
} from 'react-router-native';

import CollectionScreen from './screens/CollectionScreen';
import LibraryScreen from './screens/LibraryScreen';

export interface IProps {
  match: IMatch;
}

export class LibraryIndex extends React.PureComponent<IProps> {
  render () {
    const { match } = this.props;
    return (
      <Switch>
        <Route path={`${match.path}/:collectionId`} component={CollectionScreen} />
        <Route path={match.path} component={LibraryScreen} />
      </Switch>
    );
  }
}

export default LibraryIndex;
