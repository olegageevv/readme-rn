import React from 'react';
import { View } from 'react-native';

import { List, Search } from 'src/components';

import styles from './StoreScreen.styles';

class StoreScreen extends React.Component {
  render () {
    return (
      <View style={styles.container}>
        <Search />
        <List type="store" />
      </View>
    );
  }
}

export default StoreScreen;
