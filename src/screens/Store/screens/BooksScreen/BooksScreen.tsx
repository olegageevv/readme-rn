import { goBack } from 'connected-react-router';
import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { match as IMatch } from 'react-router-native';

import { Header, List } from 'src/components';
import IRootState, { IStoreState } from 'src/models';

import styles from './BooksScreen.styles';

interface IParams {
  categoryId: string;
}

export interface IProps {
  store: IStoreState;
  match: IMatch<IParams>;
  goToBack: () => void;
}

class BooksScreen extends React.Component<IProps> {
  onBackClick = () => {
    this.props.goToBack();
  }

  render () {
    const {
      store,
      match,
    } = this.props;
    const { categoryId } = match.params;
    const category = store.items[`id-${categoryId}`];

    return (
      <View style={styles.container}>
        <Header
          title={category.title}
          leftItems={[{ icon: 'Back', action: this.onBackClick }]}
          rightItems={[{ icon: 'Search', action: () => undefined }]}
        />
        <List
          type="books"
          ids={category.books}
          numColumns={2}
        />
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  store: state.store,
});

const mapDispatchToProps = {
  goToBack: () => goBack(),
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(BooksScreen);
