import React from 'react';
import {
  match as IMatch,
  Route,
  Switch,
} from 'react-router-native';

import BooksScreen from './screens/BooksScreen';
import CategoryScreen from './screens/CategoryScreen';
import StoreScreen from './screens/StoreScreen';

export interface IProps {
  match: IMatch;
}

export class StoreIndex extends React.PureComponent<IProps> {
  render () {
    const { match } = this.props;
    return (
      <Switch>
        <Route path={`${match.path}/:categoryId/books`} component={BooksScreen} />
        <Route path={`${match.path}/:categoryId/categories`} component={CategoryScreen} />
        <Route path={`${match.path}`} component={StoreScreen} />
      </Switch>
    );
  }
}

export default StoreIndex;
