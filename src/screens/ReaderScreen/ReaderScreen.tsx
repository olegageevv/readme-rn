import React from 'react';

import { Reader } from 'src/components';

class ReaderScreen extends React.Component {
  render () {
    return (
      <Reader />
    );
  }
}

export default ReaderScreen;
