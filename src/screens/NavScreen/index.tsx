import { push } from 'connected-react-router';
import React from 'react';
import { connect } from 'react-redux';
import {
  match as IMatch,
  Route,
  Switch,
} from 'react-router-native';

import IRootState from 'src/models';

import IntroScreen from './screens/IntroScreen';
import NavScreen from './screens/NavScreen';

export interface IProps {
  intro: boolean;
  match: IMatch;
  showIntro: () => void;
}

export class NavIndex extends React.PureComponent<IProps> {
  render () {
    const { intro, match } = this.props;
    const component = intro ? IntroScreen : NavScreen;
    return <Route path={match.path} component={component} />;
  }
}

const mapStateToProps = (state: IRootState) => ({
  intro: state.core.intro,
});

const mapDispatchToProps = {
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(NavIndex);
