import React from 'react';
import { Text } from 'react-native';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Route } from 'react-router-native';

import { Container, NavMenu } from 'src/components';
import IRootState from 'src/models';
import routes from 'src/routes';

class NavScreen extends React.Component {
  render () {
    return (
      <>
        <Container>
          {
            routes.map(item => (
              <Route
                key={item.id}
                exact={item.id === 'home'}
                path={item.link}
                component={item.component}
              />
            ))
          }
        </Container>
        <NavMenu />
      </>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
});

const mapDispatchToProps = {};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(NavScreen);
