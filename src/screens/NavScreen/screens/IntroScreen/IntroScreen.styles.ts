import { StyleSheet } from 'react-native';

import { colors, fonts, helpers } from 'src/styles';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    position: 'relative',
  },
  content: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'flex-end',
    left: 0,
    // marginTop: helpers.fullWidth * .5,
    position: 'absolute',
    right: 0,
  },
  dotStyle: {
    backgroundColor: colors.gray,
    borderRadius: 3,
    height: 6,
    width: 6,
  },
  image: {
    left: 0,
    position: 'absolute',
    resizeMode: 'center',
    right: 0,
    top: 60,
  },
  imageWrapper: {
    height: 200,
    position: 'relative',
    width: helpers.fullWidth,
  },
  loadingWrapper: {
    alignItems: 'center',
    height: 45,
    marginBottom: -20,
  },
  skipWrapper: {
    marginTop: 20,
  },
  text: {
    color: colors.navy,
    fontFamily: fonts.fontRegular,
    fontSize: 13,
    height: 120,
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: 'center',
    width: helpers.cardWidth,
  },
  title: {
    color: colors.navy,
    fontFamily: fonts.fontHeading,
    fontSize: 22,
    paddingBottom: 8,
    paddingTop: 10,
    textAlign: 'center',
    width: helpers.cardWidth,
  },
  titleWrapper: {
    height: 108,
    justifyContent: 'flex-end',
  },
});
