import { goBack } from 'connected-react-router';
import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { connect } from 'react-redux';

import { coreActions } from 'src/actions';
import { Button } from 'src/components';
import IRootState from 'src/models';
import { helpers } from 'src/styles';

import styles from './IntroScreen.styles';

interface IProps {
  disableIntro: () => void;
  goToBack: () => void;
}

interface IState {
  activeSlide: number;
}

class IntroScreen extends Component<IProps, IState> {
  state = {
    activeSlide: 0,
  };

  // tslint:disable:max-line-length
  data = [
    {
      button: 'How does it work?',
      description: 'Here you can find the best promocodes, sales, products, share interesting products with other users and get bonus and rewards for online-purchases',
      image: require('src/assets/img/intro/1.png'),
      title: 'Welcome to our service',
    },
    {
      button: 'Next',
      description: 'Here you can find the best promocodes, sales, products, share interesting products with other users and get bonus and rewards for online-purchases',
      image: require('src/assets/img/intro/2.png'),
      title: 'Step 1. Join us',
    },
    {
      button: 'Next',
      description: 'Here you can find the best promocodes, sales, products, share interesting products with other users and get bonus and rewards for online-purchases',
      image: require('src/assets/img/intro/3.png'),
      title: 'Step 2. Read a book',
    },
    {
      button: 'How to read books?',
      description: 'Here you can find the best promocodes, sales, products, share interesting products with other users and get bonus and rewards for online-purchases',
      image: require('src/assets/img/intro/4.png'),
      title: 'Step 3. Read another book',
    },
    {
      button: 'Go to get your book',
      description: 'Here you can find the best promocodes, sales, products, share interesting products with other users and get bonus and rewards for online-purchases',
      image: require('src/assets/img/intro/5.png'),
      title: 'Good boy',
    },
  ];
  // tslint:enable:max-line-length

  carousel: any;

  get pagination () {
    const { activeSlide } = this.state;
    return (
        <Pagination
          dotsLength={this.data.length}
          activeDotIndex={activeSlide}
          dotStyle={styles.dotStyle}
          inactiveDotOpacity={.4}
          inactiveDotScale={.8}
        />
    );
  }

  onSnapToItem = (index: number) => this.setState({ activeSlide: index });

  onSubmit = () => {
    if (this.state.activeSlide !== this.data.length - 1) {
      this.carousel.snapToNext();
    } else {
      this.props.disableIntro();
    }
  }

  onSkip = () => this.props.disableIntro();

  render () {
    return (
      <View style={styles.container}>
        <Carousel
          data={this.data}
          renderItem={this.renderItem}
          sliderWidth={helpers.fullWidth}
          itemWidth={helpers.fullWidth}
          itemHeight={helpers.fullHeight}
          activeSlideAlignment={'start'}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          onSnapToItem={this.onSnapToItem}
          ref={(carousel: any) => { this.carousel = carousel; }}
        />
        {this.pagination}
        <Button
          link={true}
          size="small"
          label="Skip tutorial"
          onPress={this.onSkip}
        />
      </View>
    );
  }

  private renderItem = ({ item, index }: any) => {
    return (
      <View style={styles.container}>
        <View style={styles.imageWrapper}>
          <Image
            style={styles.image}
            source={item.image}
          />
        </View>
        <View style={styles.content}>
          <Text
            style={styles.title}
            numberOfLines={2}
          >
            {item.title}
          </Text>
          <Text style={styles.text}>
            {item.description}
          </Text>
          <Button
            label={item.button}
            color="red"
            size="medium"
            full={true}
            onPress={this.onSubmit}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  goToBack: () => goBack(),
});

const mapDispatchToProps = {
  ...coreActions,
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(IntroScreen);
