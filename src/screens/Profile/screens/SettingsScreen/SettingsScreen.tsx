import { goBack } from 'connected-react-router';
import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import { Header, MenuItem } from 'src/components';
import IRootState from 'src/models';

import styles from './SettingsScreen.styles';

export interface IProps {
  goToBack: () => void;
}

class SettingsScreen extends React.Component<IProps> {
  onBackClick = () => {
    this.props.goToBack();
  }

  handleLogout = () => undefined;

  render () {
    return (
      <View style={styles.container}>
        <Header
          title="Settings"
          leftItems={[{ icon: 'Back', action: this.onBackClick }]}
        />
        <View>
          <MenuItem title="Logout" color="red" onPress={this.handleLogout} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
});

const mapDispatchToProps = {
  goToBack: () => goBack(),
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsScreen);
