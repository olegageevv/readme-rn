import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: StyleSheet.flatten([
    {
      flex: 1,
      justifyContent: 'flex-start',
    },
  ]),
});
