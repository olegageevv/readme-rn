import { push } from 'connected-react-router';
import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import IRootState from 'src/models';

import { ProfileHeader, ProfileNumbers } from '../../components';
import styles from './ProfileScreen.styles';

export interface IProps {
  goToSettings: () => void;
}

class ProfileScreen extends React.Component<IProps> {
  onSettingPress = () => {
    this.props.goToSettings();
  }

  onEditPress = () => {
    // TODO: forward to edit profile screen
  }

  render () {
    const profile = {
      firstname: 'Artem',
      image: { uri: 'https://randomuser.me/api/portraits/men/11.jpg' },
      lastname: 'Kashin',
    };
    const fullName = `${profile.firstname} ${profile.lastname}`.trim();
    return (
      <View style={styles.container}>
        <ProfileHeader
          headerProps={{
            leftItems: [{ icon: 'Settings', action: this.onSettingPress }],
            title: '',
          }}
          editButtonText="Edit profile"
          onEditButtonClick={this.onEditPress}
          userName={fullName}
          image={profile.image || require('src/assets/img/no_profile.jpg')}
        />
        <ProfileNumbers />
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
});

const mapDispatchToProps = {
  goToSettings: () => push('/profile/settings'),
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileScreen);
