import React from 'react';
import {
  match as IMatch,
  Route,
  Switch,
} from 'react-router-native';

import ProfileScreen from './screens/ProfileScreen';
import SettingsScreen from './screens/SettingsScreen';

export interface IProps {
  match: IMatch;
}

export class ProfileIndex extends React.PureComponent<IProps> {
  render () {
    const { match } = this.props;
    return (
      <Switch>
        <Route path={`${match.path}/settings`} component={SettingsScreen} />
        <Route path={match.path} component={ProfileScreen} />
      </Switch>
    );
  }
}

export default ProfileIndex;
