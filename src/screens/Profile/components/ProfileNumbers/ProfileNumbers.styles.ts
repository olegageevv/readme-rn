import { StyleSheet } from 'react-native';

import { base, colors, fonts } from 'src/styles';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingLeft: 15,
    paddingRight: 15,
  },
  item: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    marginBottom: 20,
    marginTop: 20,
    paddingLeft: 15,
    paddingRight: 15,
  },
  itemLabel: {
    color: colors.navyLighter,
    fontSize: 14,
  },
  itemValue: {
    color: colors.navy,
    fontSize: 18,
  },
});
