import * as React from 'react';
import { Text, View } from 'react-native';

import styles from './ProfileNumbers.styles';

export class ProfileNumbers extends React.PureComponent {
  render () {
    const items = [
      {
        id: 1,
        label: 'Posts',
        value: 5,
      },
      {
        id: 2,
        label: 'Followers',
        value: 8,
      },
      {
        id: 3,
        label: 'Following',
        value: 15,
      },
    ];
    return (
      <View style={styles.container}>
        {
          items.map(item => (
            <View key={item.id} style={styles.item}>
              <Text style={styles.itemValue}>{item.value}</Text>
              <Text style={styles.itemLabel}>{item.label}</Text>
            </View>
          ))
        }
      </View>
    );
  }
}

export default ProfileNumbers;
