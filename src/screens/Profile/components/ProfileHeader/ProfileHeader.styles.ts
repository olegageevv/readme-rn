import { StyleSheet } from 'react-native';

import { base, colors, fonts } from 'src/styles';

export default StyleSheet.create({
  bodyContainer: StyleSheet.flatten([
    {
      backgroundColor: colors.white,
      borderBottomColor: colors.grayLight,
      borderBottomWidth: .5,
      flexDirection: 'row',
      paddingBottom: 16,
    },
    base.contentPaddingSmall,
  ]),
  headerContainer: {
    borderBottomWidth: 0,
  },
  image: {
    borderRadius: 45,
    height: 90,
    resizeMode: 'cover',
    shadowColor: colors.navy,
    shadowOpacity: .05,
    width: 90,
    // shadowOffset: .5,
  },
  imageContainer: {
    backgroundColor: colors.grayLighter,
    borderRadius: 45,
    height: 90,
    width: 90,
  },
  rightBlock: {
    paddingLeft: 16,
  },
  userNameText: {
    color: colors.navy,
    fontFamily: fonts.fontHeading,
    fontSize: 17,
    paddingBottom: 12,
    paddingTop: 12,
  },
});
