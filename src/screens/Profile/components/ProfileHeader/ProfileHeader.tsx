import * as React from 'react';
import { Image, ImageSourcePropType, Text, View } from 'react-native';

import { Button } from 'src/components';
import Header, { IHeaderProps } from 'src/components/Base/components/Header';

import styles from './ProfileHeader.styles';

export interface IProfileHeaderProps {
  image: ImageSourcePropType;
  userName: string;
  editButtonText: string;
  onEditButtonClick: () => void;
  headerProps: Partial<IHeaderProps>;
}

export class ProfileHeader extends React.PureComponent<IProfileHeaderProps, any> {
  static defaultProps = {
    editButtonText: null,
    headerProps: {},
    image: null,
    onEditButtonClick: () => undefined,
    userName: '',
  };

  constructor (props: IProfileHeaderProps) {
    super(props);
  }

  render () {
    const {
      image,
      userName,
      editButtonText,
      onEditButtonClick,
      headerProps,
    } = this.props;
    return (
      <View>
        <Header
          title=""
          containerStyle={styles.headerContainer}
          {...headerProps}
        />
        <View style={styles.bodyContainer}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={image}
            />
          </View>
          <View style={styles.rightBlock}>
            <Text style={styles.userNameText}>{userName}</Text>
            {
              editButtonText &&
              <Button
                label={editButtonText}
                onPress={onEditButtonClick}
                size="small"
                bordered={true}
                color="navy"
              />
            }
          </View>
        </View>
      </View>
    );
  }
}

export default ProfileHeader;
