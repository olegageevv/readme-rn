import React from 'react';
import { StyleSheet } from 'react-native';
import renderer from 'react-test-renderer';

import { Button, Header } from 'src/components';

import { ProfileHeader } from '../components';

it('should render without props', () => {
  const root = renderer.create(
    <ProfileHeader />,
  ).root;
  const headerInst = root.findByType(Header);
  expect(
    StyleSheet.flatten(headerInst.props.containerStyle),
  ).toHaveProperty('borderBottomWidth', 0);
});

it('should render user name', () => {
  const userName = 'Test Testovich';
  const root = renderer.create(
    <ProfileHeader userName={userName} />,
  ).root;
  const userNameView = root.findByProps({ children: userName });
});

it('should render user pic', () => {
  const uri = 'http://test.ru/test.png';
  const root = renderer.create(
    <ProfileHeader image={{ uri }} />,
  ).root;
  const userImageView = root.findByType('Image');
  expect(userImageView.props).toHaveProperty('source.uri', uri);
});

it('should render edit profile button', () => {
  const onPressCallback = jest.fn();
  const buttonText = 'edit profile';
  const root = renderer.create(
    <ProfileHeader
      editButtonText={buttonText}
      onEditButtonClick={onPressCallback}
    />,
  ).root;
  const button = root.findByType(Button);
  expect(button.props).toHaveProperty('label', buttonText);
  button.props.onPress();
  expect(onPressCallback).toBeCalled();
});

it('should use icons header props', () => {
  const icons = {
    leftItems: [
      { action: () => undefined, icon: 'Settings' },
    ],
    rightItems: [
      { action: () => undefined, icon: 'Heart' },
    ],
  };
  const root = renderer.create(
    <ProfileHeader
      headerProps={{
        leftItems: icons.leftItems,
        rightItems: icons.rightItems,
      }}
    />,
  ).root;
  const header = root.findByType(Header);
  expect(header.props).toHaveProperty('leftItems', icons.leftItems);
  expect(header.props).toHaveProperty('rightItems', icons.rightItems);
});
