import { StyleSheet } from 'react-native';

import { colors, helpers } from 'src/styles';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.white,
    borderTopColor: colors.grayLight,
    borderTopWidth: .5,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    minHeight: 64,
    padding: 10,
    width: helpers.fullWidth,
  },
  icon: {
    position: 'absolute',
    right: 10,
    top: 10,
  },
  input: {
    minHeight: 44,
    width: helpers.cardWidth - 20,
  },
  inputBox: {
    alignItems: 'center',
    backgroundColor: colors.grayLighter,
    borderRadius: 10,
    // margin: 10,
    position: 'relative',
    width: helpers.cardWidth - 10,
  },
  sendButton: {
    alignItems: 'center',
    backgroundColor: colors.blue,
    borderRadius: 10,
    // margin: 10,
    padding: 10,
  },
});
