import React from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';

import { Icon } from 'src/components';

import styles from './MessageInput.styles';

const MAX_HEIGHT = 100;

interface IState {
  height: number;
  value: string;
}

export default class MessageInput extends React.Component<{}, IState> {
  state = {
    height: 40,
    value: '',
  };

  updateSize = (height: number) => {
    if (height < MAX_HEIGHT) this.setState({ height });
  }

  onChangeText = (value: string) => this.setState({ value });

  onContentSizeChange = (e: any) => this.updateSize(e.nativeEvent.contentSize.height);

  attachMedia = () => undefined;
  sendMessage = () => undefined;

  render () {
    const { value, height } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.inputBox}>
          <TextInput
            placeholder="Your Comment"
            onChangeText={this.onChangeText}
            style={[styles.input, { height }]}
            editable={true}
            multiline={true}
            value={value}
            onContentSizeChange={this.onContentSizeChange}
          />
          <TouchableOpacity style={styles.icon} onPress={this.attachMedia}>
            <Icon name="Attachment" fill="gray" />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.sendButton} onPress={this.sendMessage}>
          <Icon name="Send" fill="white" />
        </TouchableOpacity>
      </View>
    );
  }
}
