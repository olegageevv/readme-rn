import { StyleSheet } from 'react-native';

import { base } from 'src/styles';

export default StyleSheet.create({
  container: StyleSheet.flatten([
    base.container,
    {
      alignItems: 'center',
    },
  ]),
});
