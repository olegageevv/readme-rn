import { goBack } from 'connected-react-router';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { match as IMatch } from 'react-router-native';

import { Header, List, Message, Spacer } from 'src/components';
import IRootState, { IChannelState } from 'src/models';
import { IMessage } from 'src/models/IChannelState';

import MessageInput from '../../components/MessageInput';

import styles from './ChannelScreen.styles';

interface IParams {
  channelId: string;
}

export interface IProps {
  channels: IChannelState;
  match: IMatch<IParams>;
  goToBack: () => void;
}

class ChannelScreen extends React.Component<IProps> {
  onBackClick = () => {
    this.props.goToBack();
  }

  render () {
    const {
      channels,
      match,
    } = this.props;
    const { channelId } = match.params;
    const onPress = () => undefined;
    const bookChannel = channels.books[`id-${channelId}`];
    const channel = channels.messages[`id-${channelId}`];
    const parent = channel &&
    channel.parent &&
    bookChannel &&
    bookChannel.items.find((message: IMessage) => {
      return !!(message.channel && message.channel === channel.parent);
    });
    return (
      <View style={styles.container}>
        <Header
          title=""
          leftItems={[{ icon: 'Back', action: this.onBackClick }]}
        />
        <ScrollView>
          {parent && <Message message={parent} highlight={true} />}
          <Spacer height={10} />
          <List type="messages" channel={channel} onPress={onPress} />
        </ScrollView>
        <MessageInput />
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  channels: state.channels,
});

const mapDispatchToProps = {
  goToBack: () => goBack(),
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(ChannelScreen);
