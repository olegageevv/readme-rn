import React from 'react';
import {
  match as IMatch,
  Route,
  Switch,
} from 'react-router-native';

import ChannelScreen from './screens/ChannelScreen';

export interface IProps {
  match: IMatch;
}

export class ChannelIndex extends React.PureComponent<IProps> {
  render () {
    const { match } = this.props;
    return (
      <Switch>
        <Route path={match.path} component={ChannelScreen} />
      </Switch>
    );
  }
}

export default ChannelIndex;
