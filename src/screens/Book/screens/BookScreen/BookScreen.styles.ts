import { StyleSheet } from 'react-native';

import { base, colors, helpers } from 'src/styles';

export default StyleSheet.create({
  button: {
    alignSelf: 'center',
    bottom: 0,
    marginTop: 10,
  },
  container: StyleSheet.flatten([
    base.container,
  ]),
  footer: {
    alignItems: 'center',
    backgroundColor: colors.white,
    borderTopColor: colors.grayLight,
    borderTopWidth: .5,
    bottom: 0,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    left: 0,
    position: 'absolute',
    right: 0,
    shadowColor: colors.navy,
    shadowOffset: {
      height: -4,
      width: 0,
    },
    shadowOpacity: 0.03,
    shadowRadius: 2,
    width: helpers.fullWidth,
  },
  title: {
    marginHorizontal: 15,
  },
});
