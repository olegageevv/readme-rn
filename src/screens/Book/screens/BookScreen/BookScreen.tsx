import { goBack, push } from 'connected-react-router';
import React from 'react';
import { ScrollView, View } from 'react-native';
import { connect } from 'react-redux';
import { match as IMatch } from 'react-router-native';

import {
  Books,
  Button,
  Header,
  HorizontalScroll,
  Messages,
  Spacer,
  Title,
} from 'src/components';
import IRootState, { IBooksState, IChannelState } from 'src/models';

import { BookHeader } from '../../components';
import styles from './BookScreen.styles';

interface IParams {
  bookId: string;
}

export interface IProps {
  books: IBooksState;
  channels: IChannelState;
  match: IMatch<IParams>;
  goToBack: () => void;
  channelOpen: (channelId: string) => void;
}

class BookScreen extends React.Component<IProps> {
  onBackClick = () => {
    this.props.goToBack();
  }

  render () {
    const {
      books,
      channels,
      match,
    } = this.props;
    const { bookId } = match.params;
    const book = books.items[`id-${bookId}`];
    const channel = channels.books[`id-${bookId}`];
    const onPress = () => undefined;

    const lastBooks = Object.keys(books.items).map(key => books.items[key]);
    const onMessagePress = () => this.props.channelOpen(bookId);
    const bookOpen = () => undefined;

    return (
      <View style={styles.container}>
        <Header
          title={book.title}
          leftItems={[{ icon: 'Back', action: this.onBackClick }]}
          rightItems={[{ icon: 'Dots', action: () => undefined }]}
        />
        <ScrollView>
          <BookHeader book={book} />
          {
            channel &&
            <>
              <Title onPress={onPress}>Messages</Title>
              <HorizontalScroll>
                <Messages data={channel.items} onPress={onMessagePress} />
              </HorizontalScroll>
            </>
          }
          {
            lastBooks &&
            <>
              <Title onPress={onPress}>Linked books</Title>
              <HorizontalScroll>
                <Books data={lastBooks} onPress={bookOpen} />
              </HorizontalScroll>
            </>
          }
          <Spacer height={60} />
        </ScrollView>
        <View style={styles.footer}>
          <Button
            color="red"
            full={true}
            style={styles.button}
            label={`Buy ${book.price}`}
            onPress={onPress}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  books: state.books,
  channels: state.channels,
});

const mapDispatchToProps = {
  channelOpen: (channelId: string) => push(`/channel/${channelId}`),
  goToBack: () => goBack(),
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(BookScreen);
