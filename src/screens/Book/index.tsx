import React from 'react';
import {
  match as IMatch,
  Route,
  Switch,
} from 'react-router-native';

import BookScreen from './screens/BookScreen';

export interface IProps {
  match: IMatch;
}

export class BookIndex extends React.PureComponent<IProps> {
  render () {
    const { match } = this.props;
    return (
      <Switch>
        <Route path={match.path} component={BookScreen} />
      </Switch>
    );
  }
}

export default BookIndex;
