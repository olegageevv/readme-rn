import { StyleSheet } from 'react-native';

import { colors, fonts, helpers } from 'src/styles';

export default StyleSheet.create({
  author: {
    color: colors.blueDark,
    marginLeft: 6,
  },
  authors: {
    flexDirection: 'row',
    marginTop: 10,
  },
  book: {
    backgroundColor: colors.blueDark,
    borderRadius: 20,
    height: helpers.fullWidth * .9,
    position: 'relative',
    shadowColor: colors.navy,
    shadowOffset: {
      height: 8,
      width: 0,
    },
    shadowOpacity: .20,
    shadowRadius: 8,
    width: helpers.fullWidth * .6,
  },
  bookAuthor: {
    color: colors.white,
    fontSize: 16,
  },
  bookCover: {
    bottom: 10,
    left: 10,
    position: 'absolute',
    right: 10,
    top: 10,
  },
  bookTitle: {
    color: colors.white,
    fontFamily: fonts.fontHeading,
    fontSize: 28,
    paddingBottom: 6,
  },
  buttons: {
    marginTop: 30,
  },
  container: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginVertical: 30,
  },
  cover: {
    borderRadius: 20,
    height: helpers.fullWidth * .9,
    // width: helpers.fullWidth * .6,
  },
  description: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    lineHeight: 21,
    width: helpers.cardWidth,
  },
  general: {
    alignItems: 'center',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    marginBottom: 30,
    marginTop: 10,
    width: helpers.fullWidth - 60,
  },
  rating: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'center',
    marginVertical: 10,
  },
  title: {
    fontFamily: fonts.fontHeading,
    fontSize: 18,
    textAlign: 'center',
  },
});
