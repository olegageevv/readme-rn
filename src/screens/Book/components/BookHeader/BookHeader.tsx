import React from 'react';
import { Text, TouchableHighlight, View } from 'react-native';

import { Button, Icon } from 'src/components';

import { IBook } from 'src/models/IBooksState';
import { colors } from 'src/styles';

import styles from './BookHeader.styles';
import BookCarousel from './components/BookCarousel';
import ReadMore from './components/ReadMore';

interface IProps {
  book: IBook;
}

class BookHeader extends React.PureComponent<IProps> {
  renderRating = (rating: number = 0) => {
    const star = { name: 'Star', fill: colors.gray, width: 16, height: 16 };
    const starFull = { name: 'StarFull', fill: colors.gold };
    const stars = Array(5).fill(star).fill({ ...star, ...starFull }, 0, rating);
    return stars.map((props, i) => <Icon key={i} {...props} />);
  }

  render () {
    const {
      cover,
      images = [],
      title,
      author,
      rating,
      about,
    } = this.props.book;
    const data = [cover, ...images];
    const onShare = () => undefined;
    return (
      <View style={styles.container}>
        {
          cover
          ? <BookCarousel data={data} />
          : <View style={styles.book}>
              <View style={styles.bookCover}>
                <Text style={styles.bookTitle} numberOfLines={5}>{title}</Text>
                <Text style={styles.bookAuthor} numberOfLines={2}>{author}</Text>
              </View>
            </View>
        }
        <View style={styles.general}>
          <View style={styles.rating}>
            {this.renderRating(rating)}
          </View>
          <Text style={styles.title}>{title}</Text>
          <View style={styles.authors}>
            <Text>Автор:</Text>
            <TouchableHighlight>
              <Text style={styles.author}>{author}</Text>
            </TouchableHighlight>
          </View>
        </View>
        <View style={styles.description}>
          <ReadMore numberOfLines={4}>{about}</ReadMore>
        </View>
        <View style={styles.buttons}>
          <Button
            full={true}
            label="Share this book"
            color="gray"
            onPress={onShare}
            iconRight="Share"
          />
        </View>
      </View>
    );
  }
}

export default BookHeader;
