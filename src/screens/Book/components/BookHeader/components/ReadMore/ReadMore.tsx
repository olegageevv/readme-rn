import React from 'react';
import {
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

import styles from './ReadMore.styles';

interface IProps {
  numberOfLines: number;
  onReady?: () => void;
  renderTruncatedFooter?: (payload: () => void) => void;
  renderRevealedFooter?: (payload: () => void) => void;
}

interface IState {
  measured: boolean;
  shouldShowReadMore: boolean;
  showAll: boolean;
  isMounted: boolean;
}

type RefType = Text | View | null;

class ReadMore extends React.Component<IProps, IState> {
  state = {
    isMounted: true,
    measured: false,
    shouldShowReadMore: false,
    showAll: false,
  };

  ref: RefType = null;

  async componentDidMount () {
    const { isMounted } = this.state;

    await nextFrameAsync();
    if (!isMounted) return;

    // Get the height of the text with no restriction on number of lines
    const fullHeight = await measureHeightAsync(this.ref);
    this.setState({ measured: true });

    await nextFrameAsync();
    if (!isMounted) return;

    // Get the height of the text now that number of lines has been set
    const limitedHeight = await measureHeightAsync(this.ref);

    if (fullHeight > limitedHeight) {
      this.setState({ shouldShowReadMore: true }, this.onReady);
    } else {
      this.onReady();
    }
  }

  componentWillUnmount () {
    this.setState({ isMounted: false });
  }

  render () {
    const { measured, showAll } = this.state;
    const { numberOfLines } = this.props;
    const ref = (instance: RefType) => this.ref = instance;

    return (
      <View>
        <Text
          ref={ref}
          numberOfLines={measured && !showAll ? numberOfLines : 0}
          style={styles.text}
        >
          {this.props.children}
        </Text>
        {this.renderReadMore()}
      </View>
    );
  }

  private onReady = () => {
    if (this.props.onReady) {
      this.props.onReady();
    }
  };

  private handlePressReadMore = () => {
    this.setState({ showAll: true });
  };

  private handlePressReadLess = () => {
    this.setState({ showAll: false });
  };

  private renderReadMore () {
    const { shouldShowReadMore, showAll } = this.state;

    if (shouldShowReadMore && !showAll) {
      if (this.props.renderTruncatedFooter) {
        return this.props.renderTruncatedFooter(this.handlePressReadMore);
      }

      return (
        <TouchableOpacity onPress={this.handlePressReadMore} style={styles.more}>
          <View style={styles.moreIcon} />
        </TouchableOpacity>
      );
    }

    if (shouldShowReadMore && showAll) {
      if (this.props.renderRevealedFooter) {
        return this.props.renderRevealedFooter(this.handlePressReadLess);
      }

      return (
        <TouchableOpacity onPress={this.handlePressReadLess} style={styles.more}>
          <View style={styles.moreIcon} />
        </TouchableOpacity>
      );
    }
  }
}

const measureHeightAsync = (component: RefType) => {
  if (!component) return Promise.resolve(0);
  return new Promise(resolve => component.measure((
    x: number,
    y: number,
    w: number,
    h: number,
  ) => resolve(h)));
};

const nextFrameAsync = () => new Promise(requestAnimationFrame);

export default ReadMore;
