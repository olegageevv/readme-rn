import { StyleSheet } from 'react-native';

import { colors, helpers } from 'src/styles';

export default StyleSheet.create({
  more: {
    alignItems: 'center',
    height: 30,
    justifyContent: 'center',
    marginBottom: -30,
    width: helpers.cardWidth,
  },
  moreIcon: {
    backgroundColor: colors.gray,
    borderRadius: 2,
    height: 4,
    width: 26,
  },
  text: {
    color: colors.navyLight,
    lineHeight: 21,
  },
  view: {
    overflow: 'hidden',
  },
});
