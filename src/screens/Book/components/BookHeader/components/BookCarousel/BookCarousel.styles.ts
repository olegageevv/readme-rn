import { StyleSheet } from 'react-native';

import { helpers } from 'src/styles';

import styles from '../../BookHeader.styles';

export default StyleSheet.create({
  book: StyleSheet.flatten([
    styles.book,
  ]),
  cover: {
    borderRadius: 20,
    height: helpers.fullWidth * .9,
    // width: helpers.fullWidth * .6,
  },
  slider: {
    marginBottom: 20,
    marginTop: 10,
  },
});
