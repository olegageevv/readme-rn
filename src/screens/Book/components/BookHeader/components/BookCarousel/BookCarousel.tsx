import React from 'react';
import { Easing, View } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import ZoomImage from 'react-native-zoom-image';

import { helpers } from 'src/styles';

import styles from './BookCarousel.styles';

interface IProps {
  data: { uri: string }[];
}

class BookCarousel extends React.PureComponent<IProps> {
  render () {
    const { data } = this.props;
    return (
      <Carousel
        data={data}
        renderItem={this.renderItem}
        sliderWidth={helpers.fullWidth}
        itemWidth={helpers.fullWidth * .6}
        itemHeight={helpers.fullWidth * .9}
        contentContainerCustomStyle={styles.slider}
        inactiveSlideScale={.7}
      />
    );
  }

  private renderItem ({ item }: any) {
    return (
      <View style={styles.book}>
        <ZoomImage
          source={item}
          imgStyle={styles.cover}
          duration={200}
          enableScaling={false}
          easingFunc={Easing.ease}
        />
      </View>
    );
  }
}

export default BookCarousel;
