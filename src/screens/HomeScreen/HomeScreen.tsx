import React from 'react';
import { Text } from 'react-native';

import { Container, Header } from 'src/components';

class HomeScreen extends React.Component<{}> {
  render () {
    return (
      <>
        <Header title="Home" />
        <Container>
          <Text>1. Open current book</Text>
          <Text>2. My books/friends/authors feed updates</Text>
        </Container>
      </>
    );
  }
}

export default HomeScreen;
