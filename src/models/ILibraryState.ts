export default interface ILibraryState {
  error?: Error;
  active?: string;
  items: IBookshelves;
  loading: boolean;
  refreshing: boolean;
  total: number;
}

export interface IBookshelves {
  [collectionId: string]: ICollection;
}

export interface ICollection {
  id: string;
  image?: { uri: string };
  title: string;
  books: string[];
}
