import { RouterState } from 'connected-react-router';
import {
  IBooksState,
  IChannelState,
  ICoreState,
  ILibraryState,
  IStoreState,
} from '.';

export default interface IRootState {
  books: IBooksState;
  channels: IChannelState;
  core: ICoreState;
  library: ILibraryState;
  router: RouterState;
  store: IStoreState;
}
