import React from 'react';
export default interface ICoreState {
  loading: boolean;
  intro: boolean;
  activeMenuKey: string;
}
