export default interface IChannelState {
  messages: IChannels;
  books: IChannels;
  videos: IChannels;
  photos: IChannels;
}

export interface IChannels {
  [channelId: string]: IChannel;
}

export interface IChannel {
  error?: Error;
  items: IMessage[];
  followers?: string[];
  loading: boolean;
  parent?: string;
  refreshing: boolean;
  total: number;
}

export interface IMessage {
  id: string;
  description: string;
  time: number;
  author: string;
  type: string;
  likes?: number;
  comments?: number;
  channel?: string;
}

export type TChannel = 'books' | 'messages';
