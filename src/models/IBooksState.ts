export default interface IBooksState {
  error?: Error;
  active?: string;
  items: IBooks;
  loading: boolean;
  refreshing: boolean;
  total: number;
}

export interface IBooks {
  [bookId: string]: IBook;
}

export interface IBook {
  id: string;
  cover: { uri: string };
  images?: { uri: string }[];
  title: string;
  author: string;
  price: string;
  rating?: number;
  about?: string;
  categories?: string[];
}
