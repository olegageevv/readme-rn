export default interface IStoreState {
  error?: Error;
  active?: string;
  items: ICategories;
  loading: boolean;
  refreshing: boolean;
  total: number;
}

export interface ICategories {
  [categoryId: string]: ICategory;
}

export interface ICategory {
  id: string;
  image?: { uri: string };
  title: string;
  books: string[];
  categories?: string[];
  featured?: boolean;
}
