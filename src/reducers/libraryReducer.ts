import { libraryActions } from 'src/actions';
import { ILibraryState } from 'src/models';

export const initialState: ILibraryState = {
  items: {
    'id-1': {
      books: ['id-1', 'id-2', 'id-3'],
      id: '1',
      image: { uri: 'https://randomuser.me/api/portraits/men/20.jpg' },
      title: 'Bookshelf1',
    },
    'id-2': {
      books: ['id-4', 'id-5', 'id-6', 'id-7', 'id-8'],
      id: '2',
      image: { uri: 'https://randomuser.me/api/portraits/men/21.jpg' },
      title: 'Bookshelf2',
    },
  },
  loading: false,
  refreshing: false,
  total: 2,
};

export default (
  state: ILibraryState = initialState,
  action: libraryActions.Action,
): ILibraryState => {
  switch (action.type) {
    case libraryActions.ActionTypes.COLLECTION_OPEN: {
      const { id } = action.payload;
      state.active = id;
      return {
        ...state,
      };
    }
    default:
      return state;
  }
};
