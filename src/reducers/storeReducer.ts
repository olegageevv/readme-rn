import { storeActions } from 'src/actions';
import { IStoreState } from 'src/models';

export const initialState: IStoreState = {
  items: {
    /* tslint:disable:object-literal-sort-keys */
    'id-1': {
      books: ['id-1', 'id-2', 'id-3'],
      categories: ['id-9'],
      featured: true,
      id: '1',
      image: { uri: 'https://randomuser.me/api/portraits/men/25.jpg' },
      title: 'Free',
    },
    'id-2': {
      books: ['id-4', 'id-5', 'id-6', 'id-7', 'id-8'],
      categories: ['id-5'],
      featured: true,
      id: '2',
      image: { uri: 'https://randomuser.me/api/portraits/men/26.jpg' },
      title: 'Non-fiction',
    },
    'id-3': {
      books: ['id-4', 'id-5', 'id-6', 'id-7', 'id-8'],
      categories: ['id-7', 'id-8'],
      featured: true,
      id: '3',
      image: { uri: 'https://randomuser.me/api/portraits/men/27.jpg' },
      title: 'Biography',
    },
    'id-4': {
      books: ['id-4', 'id-5', 'id-6', 'id-7', 'id-8'],
      featured: true,
      id: '4',
      image: { uri: 'https://randomuser.me/api/portraits/men/28.jpg' },
      title: 'You may like',
    },
    'id-5': {
      books: ['id-8', 'id-9', 'id-10', 'id-1'],
      id: '5',
      image: { uri: 'https://randomuser.me/api/portraits/men/50.jpg' },
      title: 'Recommended',
    },
    'id-6': {
      books: ['id-4', 'id-5', 'id-6'],
      id: '6',
      image: { uri: 'https://randomuser.me/api/portraits/men/29.jpg' },
      title: 'Bestsellers',
    },
    'id-7': {
      books: ['id-2', 'id-6', 'id-1'],
      id: '7',
      image: { uri: 'https://randomuser.me/api/portraits/men/51.jpg' },
      title: 'New',
    },
    'id-8': {
      books: ['id-1', 'id-5', 'id-6'],
      id: '8',
      image: { uri: 'https://randomuser.me/api/portraits/men/52.jpg' },
      title: 'Bestsellers',
    },
    'id-9': {
      books: ['id-9', 'id-2'],
      id: '9',
      image: { uri: 'https://randomuser.me/api/portraits/men/53.jpg' },
      title: 'Adventures',
    },
    'id-10': {
      books: ['id-5', 'id-6', 'id-7', 'id-8', 'id-4'],
      featured: true,
      id: '10',
      image: { uri: 'https://randomuser.me/api/portraits/men/54.jpg' },
      title: 'New books',
    },
    'id-11': {
      books: ['id-6', 'id-7', 'id-8', 'id-4', 'id-5'],
      featured: true,
      id: '11',
      image: { uri: 'https://randomuser.me/api/portraits/men/55.jpg' },
      title: 'Bestsellers',
    },
    /* tslint:enable:object-literal-sort-keys */
  },
  loading: false,
  refreshing: false,
  total: 5,
};

export default (
  state: IStoreState = initialState,
  action: storeActions.Action,
): IStoreState => {
  switch (action.type) {
    case storeActions.ActionTypes.STORE_CATEGORY_OPEN: {
      const { id } = action.payload;
      state.active = id;
      return {
        ...state,
      };
    }
    default:
      return state;
  }
};
