import { connectRouter } from 'connected-react-router';
import { MemoryHistory } from 'history';
import books from './booksReducer';
import channels from './channelsReducer';
import core from './coreReducer';
import library from './libraryReducer';
import store from './storeReducer';

const rootReducer = (history: MemoryHistory) => ({
  books,
  channels,
  core,
  library,
  store,
  router: connectRouter(history),
});

export default rootReducer;
