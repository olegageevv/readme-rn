import { booksActions } from 'src/actions';
import { IBooksState } from 'src/models';

export const initialState: IBooksState = {
  items: {
    /* tslint:disable:object-literal-sort-keys max-line-length */
    'id-1': {
      about: `Книга простым языком объясняет читателю, каким образом работает человеческий мозг. Без сложной терминологии автор говорит о самой природе сознания – емко, просто и с долей юмора. Почему мы недооцениваем возможности собственного мозга, неправильно воспринимаем окружающий мир и людей в нем? А главное – почему большинству современников так тяжело найти самих себя? Ответы – в «Красной таблетке».
      Проводя аналогию с фильмом «Матрица» братьев Вачовски, Андрей Курпатов говорит о том, как одни люди управляют сознанием других, как человек идет к своему счастью и что же такое счастье.
      «Красная таблетка» содержит примеры и результаты исследований в области нейрофизиологии, нейробиологии и медицины. Но главное, книга учит грамотно управлять сознанием, избавляться от навязанных стереотипов и проявлять собственную индивидуальность.`,
      author: 'Андрей Курпатов', // TODO IAuthorState
      id: '1',
      images: [{ uri: 'https://ozon-st.cdn.ngenix.net/multimedia/1020899790.jpg' }, { uri: 'https://ozon-st.cdn.ngenix.net/multimedia/1021969597.jpg' }],
      cover: { uri: 'https://cv4.litres.ru/pub/c/elektronnaya-kniga/cover_415/32481440-andrey-kurpatov-krasnaya-tabletka-posmotri-pravde-v-glaza.jpg' },
      price: '399 ₽',
      rating: 4.38  ,
      title: 'Красная таблетка. Посмотри правде в глаза!',
    },
    'id-2': {
      about: `Человек – самый сложный механизм на планете, а йога – инструкция по его применению. Так считает Садхгуру – йог, мистик и коуч ведущих компаний мира. Эта увлекательная книга основана на разработанной им системе «Внутренняя инженерия», призванной убрать с йоги культурные наслоения и представить ее как технологию достижения радости и благополучия, доступную любому человеку. Руководствуясь этой древней мудростью, поданной в современном ключе, каждый может сделать свою жизнь именно такой, какой хочет.
      Садхгуру успешно выступал с лекциями о «Внутренней инженерии» в самых авторитетных учреждениях и университетах по всему миру, включая ООН, Google, General Electric, Сбербанк, а также Оксфорд, Стэнфорд, Гарвард, Йельский университет, Уортонскую школу бизнеса, Массачусетский технологический институт и так далее.
      Садхгуру также является основателем некоммерческой благотворительной организации Isha, насчитывающей более 7 миллионов волонтеров по всему миру. Фонду присвоен специальный консультативный статус при Экономическом и Социальном Совете ООН.`,
      author: 'Садхгуру',
      id: '2',
      cover: { uri: 'https://cv1.litres.ru/pub/c/elektronnaya-kniga/cover_415/33387215-sadhguru-15158695-vnutrennyaya-inzheneriya-put-k-radosti-prakticheskoe-ruk.jpg' },
      price: '349 ₽',
      rating: 4.24,
      title: 'Внутренняя инженерия. Путь к радости. Практическое руководство от йога',
    },
    'id-3': {
      about: `Книга-сенсация ‒ №1 среди книг по самопомощи. Нет необходимости смиряться с реальностью и постоянно подстраиваться, ведь в любой момент мы можем изменить свою жизнь. Автор мировых бестселлеров по развитию мозга, профессор нейрохимии и нейробиологии, доктор Джо Диспенза предлагает научный подход к изменению жизни. Его уникальная программа рассчитана на 4 недели, за это время она научит вас работать со своим подсознанием, чтобы достичь желаемого ‒ вам остается только решить, что конкретно вы хотите изменить в своей жизни. Эта умная, содержательная и насыщенная практическим материалом книга поможет вам освободиться из плена эмоций, наполнить жизнь здоровьем, счастьем и изобилием. Каждый, кто прочтет эту книгу и воспользуется методикой доктора Диспензы, не пожалеет о затраченных усилиях.
      Эта книга:
      доступно объяснит работу подсознания;
      изменит ваше представление о том, как устроен ваш мозг;
      научит вас проникать в сферу подсознания и перепрограммировать его;
      раскроет эффективные техники медитации.`,
      author: 'Джо Диспенза',
      id: '3',
      cover: { uri: 'https://cv4.litres.ru/pub/c/elektronnaya-kniga/cover_415/6301646-dzho-dispenza-sila-podsoznaniya-ili-kak-izmenit-zhizn-za-4-nedeli.jpg' },
      price: '299 ₽',
      rating: 4.25,
      title: 'Сила подсознания, или Как изменить жизнь за 4 недели',
    },
    'id-4': {
      about: 'Методика Scrum – решение, найденное Джеффом Сазерлендом, чтобы преодолеть классические недостатки управления проектами: отсутствие слаженной работы внутри команды, невыполнение намеченных планов, дублирование задач внутри подразделений и т. д. В отличие от старого «поэтапного» подхода, при котором выбрасываются на ветер огромные средства и который зачастую так ни к чему не приводит, Scrum позволяет выполнять обязательства меньшими силами, в короткие сроки и с низкими затратами, а итоговый продукт отличается отменным качеством. Сегодня Scrum уже прочно закрепилась в управленческом арсенале большинства технологичных компаний мира. Теперь этот инструмент повышения продуктивности доступен и вам.',
      author: 'Джефф Сазерленд',
      id: '4',
      cover: { uri: 'https://cv3.litres.ru/pub/c/elektronnaya-kniga/cover_415/11946933-dzheff-sazerlend-scrum.jpg' },
      price: '349 ₽',
      rating: 5,
      title: 'Scrum. Революционный метод управления проектами',
    },
    'id-5': {
      about: 'Наши действия и поступки определены нашими мыслями. Но всегда ли мы контролируем наше мышление? Нобелевский лауреат Даниэль Канеман объясняет, почему мы подчас совершаем нерациональные поступки и как мы принимаем неверные решения. У нас имеется две системы мышления. "Медленное" мышление включается, когда мы решаем задачу или выбираем товар в магазине. Обычно нам кажется, что мы уверенно контролируем эти процессы, но не будем забывать, что позади нашего сознания в фоновом режиме постоянно работает "быстрое" мышление - автоматическое, мгновенное и неосознаваемое…',
      author: 'Даниэль Канеман',
      id: '5',
      cover: { uri: 'https://cv6.litres.ru/pub/c/bumajnaya-kniga/cover_415/16850568-daniel-kaneman-dumay-medlenno-reshay-bystro-16850568.jpg' },
      price: '455 ₽',
      rating: 4.9,
      title: 'Думай медленно… Решай быстро',
    },
    'id-6': {
      about: `Авторы этой книги проанализировали 150 стратегических ходов, совершенных на протяжении более чем 100 лет в 30 отраслях промышленности, и сделали вывод о том, что успех достигается не в соревновании, а благодаря созданию «голубых океанов» – рынков, где нет конкуренции, зато есть новый спрос и возможности для устойчивого развития и наращивания прибыли.
      В этой книге вы найдете набор необходимых аналитических инструментов, описание всех принципов, определяющих стратегию голубого океана, а также рассказ об опасных ловушках, подстерегающих компании на пути в голубые воды.
      Новое расширенное издание легендарного бестселлера, разошедшегося тиражом 3,5 миллиона экземпляров, включает две дополнительные главы, отвечающие на самые каверзные вопросы, которые задавали авторам в течение последних десяти лет.`,
      author: 'Рене Моборн, В. Чан Ким',
      id: '6',
      cover: { uri: 'https://cv8.litres.ru/pub/c/elektronnaya-kniga/cover_415/21991988-rene-moborn-strategiy-21991988.jpg' },
      price: '399 ₽',
      title: 'Стратегия голубого океана. Как найти или создать рынок, свободный от других игроков (расширенное издание)',
    },
    'id-7': {
      about: 'Книга о предпринимательском духе, воплощенном в человеке. Создатель уникального бренда Virgin, объединяющего огромное число совершенно разнородных, но вместе с тем успешных бизнесов, продолжает радовать нас новыми достижениями и еще более дерзкими планами. Увлекательно, предельно откровенно и с мягким юмором автор рассказывает о самых значимых событиях в своей жизни: провалах и победах, огорчениях и достижениях.',
      author: 'Ричард Брэнсон',
      id: '7',
      cover: { uri: 'https://cv7.litres.ru/pub/c/elektronnaya-kniga/cover_415/152773-richard-brenson-teryaya-nevinnost-kak-ya-postroil-biznes-delaya-vse-po-svo.jpg' },
      price: '459 ₽',
      rating: 4.1,
      title: 'Теряя невинность: Как я построил бизнес, делая все по-своему и получая удовольствие от жизни',
    },
    'id-8': {
      about: 'Во-первых, эта книга излагает системный подход к определению жизненных целей, приоритетов человека. Эти цели у всех разные, но книга помогает понять себя и четко сформулировать жизненные цели. Во-вторых, книга показывает, как достигать этих целей. И в-третьих, книга показывает, как каждый человек может стать лучше. Причем речь идет не об изменении имиджа, а о настоящих изменениях, самосовершенствовании. Книга не дает простых решений и не обещает мгновенных чудес. Любые позитивные изменения требуют времени, работы и упорства. Но для людей, стремящихся максимально реализовать потенциал, заложенный в них природой, эта книга – дорожная карта.',
      author: 'Стивен Кови',
      id: '8',
      cover: { uri: 'https://cv8.litres.ru/pub/c/elektronnaya-kniga/cover_415/4239285--.jpg' },
      price: '710 ₽',
      rating: 4.25,
      title: '7 навыков высокоэффективных людей: Мощные инструменты развития личности',
    },
    'id-9': {
      about: 'Казалось бы, термин «большие данные» понятен и доступен только специалистам. Но автор этой книги доказывает, что анализ данных можно организовать и в простом, понятном, очень эффективном и знакомом многим Excel. Причем не важно, сколько велик ваш массив данных. Техники, предложенные в этой книге, будут полезны и владельцу небольшого интернет-магазина, и аналитику крупной торговой компании. Вы перестанете бояться больших данных, научитесь видеть в них нужную вам информацию и сможете проанализировать предпочтения ваших клиентов и предложить им новые продукты, оптимизировать денежные потоки и складские запасы, другими словами, повысите эффективность работы вашей организации. Книга будет интересна маркетологам, бизнес-аналитикам и руководителям разных уровней, которым важно владеть статистикой для прогнозирования и планирования будущей деятельности компаний.',
      author: 'Джон Форман',
      id: '9',
      cover: { uri: 'https://cv8.litres.ru/pub/c/elektronnaya-kniga/cover_415/16898688-dzhon-forman-mnogo-cifr-analiz-bolshih-dannyh-pri-pomoschi-excel.jpg' },
      price: '2 375 ₽',
      rating: 2.67,
      title: 'Много цифр. Анализ больших данных при помощи Excel',
    },
    'id-10': {
      about: `В этой книге экономист из Кембриджа Ха-Джун Чанг в занимательной и доступной форме объясняет, как на самом деле работает мировая экономика. Автор предлагает читателю идеи, которых не найдешь в учебниках по экономике, и делает это с глубоким знанием истории, остроумием и легким пренебрежением к традиционным экономическим концепциям.
      Книга будет полезной для тех, кто интересуется экономикой и хочет лучше понимать, как устроен мир.`,
      author: 'Ха-Джун Чанг',
      id: '10',
      cover: { uri: 'https://cv5.litres.ru/pub/c/elektronnaya-kniga/cover_415/9361857-ha-dzhun-chang-kak-ustroena-ekonomika.jpg' },
      price: '132 ₽',
      rating: 4.29,
      title: 'Как устроена экономика',
    },
    /* tslint:enable:object-literal-sort-keys max-line-length */
  },
  loading: false,
  refreshing: false,
  total: 10,
};

export default (state: IBooksState = initialState, action: booksActions.Action): IBooksState => {
  switch (action.type) {
    case booksActions.ActionTypes.BOOK_OPEN: {
      const { id } = action.payload;
      state.active = id;
      return {
        ...state,
      };
    }
    default:
      return state;
  }
};
