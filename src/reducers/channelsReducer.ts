import { channelActions } from 'src/actions';
import { IChannelState } from 'src/models';

export const initialState: IChannelState = {
  books: {
    'id-1': {
      items: [
        {
          author: 'Artem Kashin',
          channel: 'id-1',
          comments: 2,
          description: 'Good book',
          id: '1',
          likes: 15,
          time: 1543868071496,
          type: 'message',
        },
        {
          author: 'Daniil Hripko',
          description: 'Yes, I like it too!',
          id: '2',
          time: 1543868423314,
          type: 'message',
        },
      ],
      loading: false,
      refreshing: false,
      total: 2,
    },
  },
  messages: {
    'id-1': {
      items: [
        {
          author: 'Daniil Hripko',
          description: 'This is a good book',
          id: '3',
          time: 1543868423314,
          type: 'message',
        },
        {
          author: 'Artem Kashin',
          description: 'Yep!',
          id: '4',
          likes: 2,
          time: 1543868071496,
          type: 'message',
        },
      ],
      loading: false,
      parent: 'id-1',
      refreshing: false,
      total: 2,
    },
  },
  photos: {},
  videos: {},
};

export default (
  state: IChannelState = initialState,
  action: channelActions.Action,
): IChannelState => {
  switch (action.type) {
    case channelActions.ActionTypes.MESSAGE_POST: {
      const { message, channelId, type } = action.payload;
      if (!state[type]) state[type] = {};
      if (!state[type]![channelId]) {
        state[type]![channelId] = {
          items: [message],
          loading: false,
          refreshing: false,
          total: 1,
        };
      } else {
        state[type]![channelId].items.push(message);
        state[type]![channelId].total += 1;
      }
      return {
        ...state,
      };
    }
    default:
      return state;
  }
};
