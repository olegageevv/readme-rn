import { coreActions } from 'src/actions';
import { ICoreState } from 'src/models';

export const initialState: ICoreState = {
  activeMenuKey: 'home',
  intro: true,
  loading: false,
};

export default (state: ICoreState = initialState, action: coreActions.Action): ICoreState => {
  switch (action.type) {
    case coreActions.ActionTypes.SET_LOADING: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case coreActions.ActionTypes.DISABLE_INTRO: {
      return {
        ...state,
        intro: false,
      };
    }
    default:
      return state;
  }
};
