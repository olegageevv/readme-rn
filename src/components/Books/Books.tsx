import React from 'react';
import { View } from 'react-native';

import { Book } from 'src/components';
import { IBook } from 'src/models/IBooksState';

import styles from './Books.styles';

interface IProps {
  data: IBook[];
  onPress: (bookId: string) => void;
}

class Books extends React.PureComponent<IProps> {
  render () {
    const {
      data,
    } = this.props;
    return data.map((item) => {
      const onPress = () => this.props.onPress(item.id);
      return (
        <View key={item.id}>
          <Book {...item} onPress={onPress} />
        </View>
      );
    });
  }
}

export default Books;
