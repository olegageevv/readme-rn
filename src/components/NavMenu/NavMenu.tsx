import { push } from 'connected-react-router';
import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import IRootState from 'src/models';
import routes, { IMenuItem } from 'src/routes';
import { getActiveNavMenuItemKey } from 'src/selectors';

import MenuItem from './components/MenuItem';
import styles from './NavMenu.styles';

interface IProps {
  activeItemKey: string;
  setMenuActive: (menuItem: IMenuItem) => void;
}

class NavMenu extends React.Component<IProps> {
  render () {
    const {
      activeItemKey,
      setMenuActive,
    } = this.props;
    return (
      <View style={styles.container}>
        {routes.map((item) => {
          const onPress = () => setMenuActive({ ...item });
          return (
            <MenuItem
              key={item.id}
              onPress={onPress}
              {...{
                ...item,
                active: item.id === activeItemKey,
              }}
            />
          );
        })}
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
  activeItemKey: getActiveNavMenuItemKey(state),
});

const mapDispatchToProps = {
  setMenuActive: (menuItem: IMenuItem) => push(menuItem.link),
};

export default connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(NavMenu);
