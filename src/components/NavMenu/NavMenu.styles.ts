import { StyleSheet } from 'react-native';

import { colors } from 'src/styles';

export default StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    borderTopColor: colors.grayLight,
    borderTopWidth: .5,
    flexDirection: 'row',
    height: 60,
    justifyContent: 'space-around',
    shadowColor: colors.navy,
    shadowOffset: {
      height: -4,
      width: 0,
    },
    shadowOpacity: 0.03,
    shadowRadius: 2,
  },
});
