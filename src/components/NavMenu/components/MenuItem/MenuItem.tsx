import React from 'react';
import { Text, View } from 'react-native';
import { Link } from 'react-router-native';

import Icon from 'src/components/Icon';
import { colors } from 'src/styles';
import styles from './MenuItem.styles';

interface IProps {
  icon: string;
  title: string;
  link: string;
  active: boolean;
  onPress: () => void;
}

class MenuItem extends React.PureComponent<IProps> {
  render () {
    const {
      icon,
      title,
      link,
      active,
      onPress,
    } = this.props;

    const iconProps = {
      fill: active ? colors.blueDark : colors.gray,
      height: 24,
      name: icon,
      viewBox: '0 0 24 24',
      width: 24,
    };

    return (
      <Link
        to={link}
        underlayColor={colors.white}
        style={styles.container}
        onPress={onPress}
      >
        <View style={styles.container}>
          {icon ? <Icon {...iconProps} /> : <Text>{title}</Text>}
        </View>
      </Link>
    );
  }
}

export default MenuItem;
