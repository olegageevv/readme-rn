import { push } from 'connected-react-router';
import React from 'react';
import { Text, TouchableHighlight, View } from 'react-native';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Icon } from 'src/components';
import IRootState from 'src/models';

import styles from './SearchButton.styles';

interface IProps {
  onPress: () => void;
}

class SearchButton extends React.Component<IProps> {
  render () {
    const {
      onPress,
    } = this.props;
    const iconProps = { height: 16, width: 16 };
    return (
      <View style={styles.container}>
        <TouchableHighlight onPress={onPress}>
          <View style={styles.input}>
            <View style={styles.icon}>
              <Icon name="Search" {...iconProps} />
            </View>
            <Text style={styles.inputField}>
              Search
            </Text>
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

const mapStateToProps = (state: IRootState) => ({
});

const mapDispatchToProps = {
  onPress: () => push('/search'),
};

export default withRouter(connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(SearchButton));
