import { StyleSheet } from 'react-native';

import { colors, helpers } from 'src/styles';

const BUTTON_WIDTH = 70;
const HEADER_HEIGHT = 44;
const INPUT_FIELD_WIDTH = helpers.cardWidth - 64;

export default StyleSheet.create({
  cancelButton: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 8,
    width: BUTTON_WIDTH,
  },
  cancelButtonText: {
    color: colors.gray,
  },
  container: {
    alignItems: 'center',
    backgroundColor: colors.white,
    borderBottomColor: colors.grayLight,
    borderBottomWidth: .5,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    height: HEADER_HEIGHT,
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    width: helpers.fullWidth,
  },
  icon: {
    marginHorizontal: 8,
  },
  input: {
    alignItems: 'center',
    backgroundColor: colors.grayLighter,
    borderRadius: 10,
    flexDirection: 'row',
    height: 40,
    marginBottom: 5,
    width: helpers.cardWidth,
  },
  inputField: {
    color: colors.gray,
    paddingVertical: 9,
    width: INPUT_FIELD_WIDTH,
  },
  inputFieldFocused: {
    width: INPUT_FIELD_WIDTH - BUTTON_WIDTH,
  },
  inputFocused: {
    width: helpers.cardWidth - BUTTON_WIDTH,
  },
  resultContainer: {
    backgroundColor: colors.grayLighter,
    height: helpers.fullHeight - HEADER_HEIGHT - 78,
    width: helpers.fullWidth,
  },
});
