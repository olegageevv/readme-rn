import { StyleSheet } from 'react-native';

import { colors, fonts, helpers } from 'src/styles';

export default StyleSheet.create({
  container: {
    alignSelf: 'center',
  },
  description: {
    color: colors.gray,
  },
  image: {
    height: helpers.fullWidth * .6,
    marginBottom: 20,
    width: helpers.fullWidth * .6,
  },
  title: {
    color: colors.gray,
    fontFamily: fonts.fontHeading,
  },
});
