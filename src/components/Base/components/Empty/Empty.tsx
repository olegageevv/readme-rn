import React from 'react';
import { Image, Text, View } from 'react-native';

import styles from './Empty.styles';

interface IProps {
  title?: string;
  description?: string;
  image?: string;
}

class Empty extends React.PureComponent<IProps> {
  render () {
    const { description, image, title } = this.props;
    return (
      <View style={styles.container}>
        {image && <Image source={{ uri: image }} style={styles.image} />}
        {title && <Text style={styles.title}>{title}</Text>}
        {description && <Text style={styles.description}>{description}</Text>}
      </View>
    );
  }
}

export default Empty;
