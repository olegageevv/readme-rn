import React, { ReactNode, ReactNodeArray } from 'react';
import { View } from 'react-native';

import styles from './Container.styles';

interface IProps {
  children: ReactNode | ReactNodeArray;
}

class Container extends React.Component<IProps> {
  render () {
    const { children } = this.props;
    return (
      <View style={styles.container}>
        {children}
      </View>
    );
  }
}

export default Container;
