import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import { colors } from 'src/styles';

import styles from './Title.styles';

export type TitleColor =
  'blue'
  | 'red'
  | 'gray'
  | 'green'
  | 'navy';
interface IProps {
  color: TitleColor;
  children: string;
  onPress?: () => void;
}

class Title extends React.PureComponent<IProps> {
  static defaultProps = {
    color: 'navy',
  };

  render () {
    const { color, children, onPress } = this.props;
    return (
      <View style={styles.container}>
        <Text style={[styles.title, { color: this.getColor(color) }]}>
          {children}
        </Text>
        {
          onPress &&
          <TouchableOpacity onPress={onPress}>
            <Text style={styles.textMore}>View all</Text>
          </TouchableOpacity>
        }
      </View>
    );
  }
  private getColor (color: string): string {
    return (colors as any)[color];
  }
}

export default Title;
