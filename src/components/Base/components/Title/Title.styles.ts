import { StyleSheet } from 'react-native';

import { colors, fonts, helpers } from 'src/styles';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30,
    paddingTop: 15,
  },
  textMore: {
    color: colors.gray,
    fontSize: 12,
  },
  title: {
    color: colors.navy,
    fontFamily: fonts.fontHeading,
    fontSize: 16,
    paddingBottom: 8,
    paddingTop: 10,
  },
});
