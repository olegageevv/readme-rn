import React, { ReactElement } from 'react';
import { LayoutChangeEvent, StyleSheet, ViewStyle } from 'react-native';
import { BoxShadow } from 'react-native-shadow';

import { colors, helpers } from 'src/styles';

export interface IShadowProps {
  border: number;
  color: string;
  height: number;
  opacity: number;
  radius: number;
  width: number;
  x: number;
  y: number;
}

interface ISize {
  height: number;
  width: number;
}

interface IState {
  size: ISize;
}

export class Shadow extends React.PureComponent<IShadowProps, IState> {
  static defaultProps = {
    border: 10,
    color: colors.navyDark,
    height: undefined,
    opacity: .12,
    radius: 10,
    width: undefined,
    x: 0,
    y: 8,
  };

  constructor (props: IShadowProps) {
    super(props);

    this.state = {
      size: {
        height: props.height,
        width: props.width,
      },
    };
  }

  getSizeFromChild (child: ReactElement<any>): ISize {
    const childStyle = StyleSheet.flatten(child.props.style) as ViewStyle || {};
    const { width, height } = childStyle;
    return {
      height: typeof height === 'number' ? height : helpers.fullHeight,
      width: typeof width === 'number' ? width : helpers.fullWidth,
    };
  }

  onChildLayout = (event: LayoutChangeEvent) => {
    const { width, height } = event.nativeEvent.layout;
    this.setState({ size: { width, height } });
  }

  render () {
    const { children } = this.props;
    const stateSize = this.state.size;
    const child = React.Children.only(children);
    let { width, height } = this.getSizeFromChild(child);
    width = this.props.width || width || stateSize.width;
    height = this.props.height || height || stateSize.height;
    return (
      <BoxShadow setting={{ ...this.props, ...{ width, height } }}>
        {React.cloneElement(child, {
          onLayout: this.onChildLayout,
        })}
      </BoxShadow>
    );
  }
}

export default Shadow;
