import React from 'react';
import { LayoutChangeEvent, View } from 'react-native';
import { BoxShadow } from 'react-native-shadow';
import renderer from 'react-test-renderer';

import { Shadow } from '../Shadow';

it('should set size from props', () => {
  const testRenderer = renderer.create(
    <Shadow width={2} height={1}>
      <View />
    </Shadow>,
  );
  const root = testRenderer.root;

  expect(root.findByType(BoxShadow).props.setting)
    .toMatchObject({
      height: 1,
      width: 2,
    });

  testRenderer.update(
    <Shadow width={4} height={3}>
      <View style={{ width: 200, height: 100 }} />
    </Shadow>,
  );

  expect(root.findByType(BoxShadow).props.setting)
    .toMatchObject({
      height: 3,
      width: 4,
    });
});

it('should get size from child style', () => {
  const size = { width: 200, height: 100 };
  const getSizeFromChild = jest.spyOn(Shadow.prototype, 'getSizeFromChild');
  const root = renderer.create(
    <Shadow>
      <View style={size} testID="childView" />
    </Shadow>,
  ).root;
  expect(getSizeFromChild).toBeCalled();
  expect(getSizeFromChild.mock.results[0].value).toMatchObject(size);

  const shadowBox = root.findByType(BoxShadow);
  expect(shadowBox.props.setting).toMatchObject(size);
});

it('should change state on layout child event', () => {
  const size = { width: 200, height: 100 };
  const root = renderer.create(
    <Shadow>
      <View testID="childView" />
    </Shadow>,
  ).root;
  const childView = root.findByProps({ testID: 'childView' });
  const event: LayoutChangeEvent = {
    nativeEvent: {
      layout: {
        x: 0,
        y: 0,
        ...size,
      },
    },
  };
  childView.props.onLayout(event);
  expect(root.instance.state.size).toMatchObject(size);
});
