import { StyleSheet } from 'react-native';

import { colors, helpers } from 'src/styles';

export default StyleSheet.create({
  button: {
    alignItems: 'center',
    height: 44,
    justifyContent: 'center',
    width: 44,
  },
  container: {
    alignItems: 'center',
    backgroundColor: colors.white,
    borderBottomColor: colors.grayLight,
    borderBottomWidth: .5,
    flexDirection: 'row',
    flexWrap: 'nowrap',
    height: 44,
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
    width: helpers.fullWidth,
  },
  left: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'center',
  },
  right: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    textAlign: 'center',
  },
  titleWrapper: {
    alignItems: 'center',
    flex: 1,
    height: 44,
    justifyContent: 'center',
  },
});
