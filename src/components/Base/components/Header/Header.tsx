import React from 'react';
import { RegisteredStyle, Text, TouchableOpacity, View, ViewStyle } from 'react-native';

import Icon from 'src/components/Icon';

import styles from './Header.styles';

interface IHeaderIcon {
  icon: string;
  color?: string;
  action: () => void;
}

export interface IProps {
  title: string;
  leftItems: IHeaderIcon[];
  rightItems: IHeaderIcon[];
  containerStyle: RegisteredStyle<ViewStyle>;
}

class Header extends React.PureComponent<IProps> {
  static defaultProps = {
    containerStyle: {},
    leftItems: [],
    rightItems: [],
  };

  renderItems = (items: IHeaderIcon[] | undefined) => {
    if (!items || !items.length) return <View style={styles.button} />;
    return items.map(item => (
      <TouchableOpacity key={item.icon} onPress={item.action} style={styles.button}>
        <Icon name={item.icon} fill={item.color} />
      </TouchableOpacity>
    ));
  };

  render () {
    const {
      title,
      leftItems,
      rightItems,
      containerStyle,
    } = this.props;
    return (
      <View style={[styles.container, containerStyle]}>
        {this.renderItems(leftItems)}
        <View style={styles.titleWrapper}>
          <Text style={styles.title} numberOfLines={1}>{title}</Text>
        </View>
        {this.renderItems(rightItems)}
      </View>
    );
  }
}

export default Header;
