import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  reader: {
    alignSelf: 'stretch',
    flex: 1,
  },
});
