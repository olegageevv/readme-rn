import { Epub, Streamer } from 'epubjs-rn';
import React from 'react';
import { ActionSheetIOS } from 'react-native';

import styles from './Reader.styles';

class Reader extends React.Component<{}> {
  state = {
    flow: 'paginated',
    location: 6,
    origin: '',
    showBars: true,
    showNav: false,
    sliderDisabled: true,
    src: '',
    title: '',
    toc: [],
    url: 'https://s3.amazonaws.com/epubjs/books/moby-dick.epub',
  };
  streamer = new Streamer();
  rendition = {};
  epub: any = {};

  componentDidMount () {
    this.streamer.start()
      .then((origin: any) => {
        // console.log('origin', origin);
        this.setState({ origin });
        return this.streamer.get(this.state.url);
      })
      .then((src: any) => {
        // console.log('src', src);
        return this.setState({ src });
      });

    setTimeout(() => this.toggleBars(), 1000);
  }

  componentWillUnmount () {
    this.streamer.kill();
  }

  toggleBars () {
    this.setState({ showBars: !this.state.showBars });
  }

  onLocationChange = (visibleLocation: any) => {
    // console.log('onLocationChange', visibleLocation, this.rendition);
    this.setState({ visibleLocation });
  }

  onLocationsReady = (locations: any) => {
    // console.log('location total', locations.total);
    this.setState({ sliderDisabled : false });
  }

  onReady = (book: any) => {
    // console.log('onReady', this, book);
    // console.log('Metadata', book.package.metadata);
    // console.log('Table of Contents', book.toc);
    this.setState({
      title : book.package.metadata.title,
      toc: book.navigation.toc,
    });
    if (this.epub && this.epub.rendition) {
      // console.log('this.epub.rendition', this.epub.rendition)
      this.epub.rendition.highlight(
        'epubcfi(/6/14[xchapter_001]!/4/2/4/8[c001s0004],/1:0,/1:8)',
        {},
      );
    }
  }

  onPress = (cfi: any, position: any, rendition: any) => {
    this.toggleBars();
    // console.log('onPress', cfi, position, rendition);
  };

  onLongPress = (cfi: any, rendition: any) => {
    // console.log('onLongPress', cfi, rendition);
  };

  onViewAdded = (index: any) => {
    // console.log('onViewAdded', index);
  };

  beforeViewRemoved = (index: any) => {
    // console.log('beforeViewRemoved', index);
  };

  onSelected = (cfiRange: any, rendition: any) => {
    // console.log('onSelected', cfiRange, rendition);
    // Add marker
    rendition.highlight(cfiRange, {});
  };

  onMarkClicked = (cfiRange: any) => {
    // console.log('onMarkClicked', cfiRange);
  };

  onError = (message: any) => {
    // console.log('onError', message);
  };

  render () {
    // console.log('this.epub', this.epub);
    return (
      <Epub
        style={styles.reader}
        // ref="epub"
        ref={(epub: any) => {
          this.epub = epub;
        }}
        src={this.state.src}
        flow={this.state.flow}
        location={this.state.location}
        onLocationChange={this.onLocationChange}
        onLocationsReady={this.onLocationsReady}
        onReady={this.onReady}
        onPress={this.onPress}
        onLongPress={this.onLongPress}
        onViewAdded={this.onViewAdded}
        beforeViewRemoved={this.beforeViewRemoved}
        onSelected={this.onSelected}
        onMarkClicked={this.onMarkClicked}
        // themes={{
        //   tan: {
        //     body: {
        //       "-webkit-user-select": "none",
        //       "user-select": "none",
        //       "background-color": "tan"
        //     }
        //   }
        // }}
        // theme="tan"
        // regenerateLocations={true}
        // generateLocations={true}
        origin={this.state.origin}
        onError={this.onError}
      />
    );
  }
}

export default Reader;
