import React from 'react';
import {
  Animated,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

import { Icon } from 'src/components';
import { colors } from 'src/styles';

import styles from './Button.styles';

export type ButtonColor =
  'blue'
  | 'red'
  | 'gray'
  | 'green'
  | 'navy';

export interface IProps {
  bordered?: boolean;
  full?: boolean;
  link?: boolean;
  label: string;
  color?: ButtonColor;
  size?: 'medium' | 'small';
  onPress: () => any;
  style?: {};
  iconRight?: string;
}

interface IState {
  scale: Animated.Value;
  animatedColor: Animated.Value;
  buttonColor: string;
  textColor: string;
  styleContainer: IStyleContainer;
}

interface IStyleContainer {
  backgroundColor: string | Animated.AnimatedInterpolation;
  borderColor: string | Animated.AnimatedInterpolation;
  transform?: [{ scale: Animated.Value }];
}

class Button extends React.PureComponent<IProps, IState> {
  static defaultProps: Partial<IProps> = {
    bordered: false,
    color: 'blue',
    full: false,
    link: false,
    size: 'medium',
  };

  constructor (props: IProps) {
    super(props);
    const { color, bordered, link } = props;
    const buttonColor: string = color as ButtonColor;
    const styleContainer: IStyleContainer = {
      backgroundColor: bordered || link ? colors.transparent : this.getColor(buttonColor),
      borderColor: link ? colors.transparent : this.getColor(buttonColor),
    };
    const textColor = bordered || link ? this.getColor(buttonColor) : colors.white;
    this.state = {
      buttonColor,
      styleContainer,
      textColor,
      animatedColor: new Animated.Value(0),
      scale: new Animated.Value(1),
    };
  }

  getColor (color: string): string {
    return (colors as any)[color];
  }

  onPressIn = () => {
    const { link } = this.props;
    const {
      buttonColor,
      styleContainer,
      scale,
      animatedColor,
    } = this.state;

    scale.setValue(1);
    Animated
      .parallel([
        Animated.timing(scale, { toValue: 0.98, duration: 150 }),
        Animated.timing(animatedColor, { toValue: 300, duration: 150 }),
      ])
      .start();

    const interpolateColor = link ? colors.transparent : animatedColor.interpolate({
      inputRange: [0, 300],
      outputRange: [this.getColor(buttonColor), this.getColor(`${buttonColor}Dark`)],
    });

    this.setState({
      styleContainer: {
        ...styleContainer,
        backgroundColor: interpolateColor,
        borderColor: interpolateColor,
        transform: [{ scale }],
      },
      textColor: link ? this.getColor(`${buttonColor}Dark`) : colors.white,
    });
  };

  onPressOut = () => {
    const { bordered, link } = this.props;
    const {
      buttonColor,
      styleContainer,
      scale,
      animatedColor,
    } = this.state;

    scale.setValue(0.98);
    Animated
      .parallel([
        Animated.timing(scale, { toValue: 1, duration: 150 }),
        Animated.timing(animatedColor, { toValue: 0, duration: 150 }),
      ])
      .start();

    const interpolateBorder = link ? colors.transparent : animatedColor.interpolate({
      inputRange: [0, 300],
      outputRange: [this.getColor(buttonColor), this.getColor(`${buttonColor}Dark`)],
    });

    const interpolateBackground = link ? colors.transparent : animatedColor.interpolate({
      inputRange: [0, 300],
      outputRange: [
        bordered ? colors.white : this.getColor(buttonColor),
        this.getColor(`${buttonColor}Dark`),
      ],
    });

    this.setState({
      styleContainer: {
        ...styleContainer,
        backgroundColor: interpolateBackground,
        borderColor: interpolateBorder,
        transform: [{ scale }],
      },
      textColor: bordered || link ? this.getColor(buttonColor) : colors.white,
    });
  };

  getTextStyle () {
    const { size } = this.props;
    const { textColor } = this.state;
    const styleText = size === 'small' ? styles.textSmall : styles.textMedium;
    return [styles.text, styleText, { color: textColor }];
  }

  render () {
    const { label, full, style, iconRight, ...rest } = this.props;
    const { styleContainer, textColor } = this.state;
    const iconProps = { fill: textColor, width: 18, height: 18 };
    return (
      <Animated.View style={[styles.container, styleContainer, full && styles.full, style]}>
        <TouchableWithoutFeedback
          {...rest}
          onPressIn={this.onPressIn}
          onPressOut={this.onPressOut}
        >
          <View style={styles.textContainer}>
            <Text style={this.getTextStyle()}>
              {label}
            </Text>
            {
              !!iconRight &&
              <View style={styles.icon}>
                <Icon name={iconRight} {...iconProps} />
              </View>
            }
          </View>
        </TouchableWithoutFeedback>
      </Animated.View>
    );
  }
}

export default Button;
