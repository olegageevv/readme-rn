import { StyleSheet } from 'react-native';

import { helpers } from 'src/styles';

export default StyleSheet.create({
  container: {
    borderRadius: 6,
    borderWidth: 1,
    marginBottom: 4,
    marginTop: 4,
  },
  full: {
    width: helpers.cardWidth,
  },
  icon: {
  },
  text: {
    alignSelf: 'center',
    fontFamily: 'Montserrat-Bold',
  },
  textContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textMedium: {
    fontSize: 16,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
  },
  textSmall: {
    fontSize: 14,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
  },
});
