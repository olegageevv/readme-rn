import * as React from 'react';
import { StyleSheet, TouchableWithoutFeedback } from 'react-native';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

import { colors, helpers } from 'src/styles';
import Button, { ButtonColor, IProps } from '../Button';
import styles from '../Button.styles';

it('render blue button with border', () => {
  const props: IProps = {
    bordered: true,
    color: 'blue',
    label: 'Test',
    onPress: () => null,
    size: 'medium',
  };
  const testRenderer = renderer.create(
    <Button {...props} />,
  );

  const tree = testRenderer.toJSON();
  expect(tree).toMatchSnapshot();

  const instance = testRenderer.getInstance();

  if (instance && tree) {
    expect(instance).toHaveProperty('props.color', props.color);
    expect(tree).toHaveProperty('props.style');

    const totalContainerStyle = StyleSheet.flatten(tree.props.style);
    expect(totalContainerStyle)
      .toHaveProperty('borderColor', colors[instance.props.color as ButtonColor]);
    expect(totalContainerStyle).toHaveProperty('backgroundColor', colors.transparent);
  }
});

it('render default color button without border', () => {
  const props: IProps = {
    bordered: false,
    label: 'Test',
    onPress: () => null,
    size: 'medium',
  };
  const testRenderer = renderer.create(
    <Button {...props} />,
  );

  const tree = testRenderer.toJSON();
  expect(tree).toMatchSnapshot();

  const instance = testRenderer.getInstance();

  if (instance && tree) {
    expect(tree).toHaveProperty('props.style');
    const totalContainerStyle = StyleSheet.flatten(tree.props.style);
    expect(totalContainerStyle)
      .toHaveProperty('borderColor', colors[instance.props.color as ButtonColor]);
    expect(totalContainerStyle)
      .toHaveProperty('backgroundColor', colors[instance.props.color as ButtonColor]);
  }
});

it('render small button', () => {
  const props: IProps = {
    label: 'Test',
    onPress: () => null,
    size: 'small',
  };
  const testRenderer = renderer.create(
    <Button {...props} />,
  );

  const tree = testRenderer.toJSON();
  expect(tree).toMatchSnapshot();

  const root = testRenderer.root;
  const textComponent = root.findByProps({ children: props.label });
  const textStyle = StyleSheet.flatten(textComponent.props.style);
  expect(textStyle).toMatchObject(styles.textSmall);
});

it('render full button', () => {
  const props: IProps = {
    full: true,
    label: 'Test',
    onPress: () => null,
  };
  const testRenderer = renderer.create(
    <Button {...props} />,
  );

  const tree = testRenderer.toJSON();

  if (tree) {
    const totalContainerStyle = StyleSheet.flatten(tree.props.style);
    expect(totalContainerStyle)
      .toHaveProperty('width', helpers.cardWidth);
  }
});

it('should call onPress handler', () => {
  const props: IProps = {
    label: 'Test',
    onPress: jest.fn(),
  };
  const root = renderer.create(
    <Button {...props} />,
  ).root;
  const innerComponent = root.findByType(TouchableWithoutFeedback);
  expect(innerComponent).toHaveProperty('props.onPress');
  innerComponent.props.onPress();
  expect(props.onPress).toBeCalled();
});
