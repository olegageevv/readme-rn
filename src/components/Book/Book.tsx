import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';

import { Icon } from 'src/components';
import { IBook } from 'src/models/IBooksState';

import styles from './Book.styles';

interface IProps extends IBook {
  onPress: (bookId: string) => void;
}

class Book extends React.Component<IProps> {
  render () {
    const {
      id,
      cover,
      price,
      title,
      author,
    } = this.props;
    const onPress = () => this.props.onPress(id);
    return (
      <TouchableOpacity onPress={onPress} style={styles.container}>
        <View>
          <View style={styles.book}>
            <View style={styles.bookCover}>
              <Text style={styles.bookTitle} numberOfLines={5}>{title}</Text>
              <Text style={styles.bookAuthor} numberOfLines={2}>{author}</Text>
            </View>
            {cover && <Image source={cover} style={styles.cover} />}
          </View>
          <View style={styles.infoWrapper}>
            <Text style={styles.price}>{price}</Text>
            <Icon name="Dots" width={20} height={20} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

export default Book;
