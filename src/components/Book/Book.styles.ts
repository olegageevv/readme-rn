import { StyleSheet } from 'react-native';

import { colors, fonts, helpers } from 'src/styles';

export default StyleSheet.create({
  book: {
    backgroundColor: colors.blueDark,
    borderRadius: 10,
    height: helpers.fullWidth * .55,
    position: 'relative',
    shadowColor: colors.navy,
    shadowOffset: {
      height: 8,
      width: 0,
    },
    shadowOpacity: 0.20,
    shadowRadius: 8,
    width: helpers.fullWidth * .35,
  },
  bookAuthor: {
    color: colors.white,
    fontSize: 12,
  },
  bookCover: {
    bottom: 10,
    left: 10,
    position: 'absolute',
    right: 10,
    top: 10,
  },
  bookTitle: {
    color: colors.white,
    fontFamily: fonts.fontHeading,
    fontSize: 18,
    paddingBottom: 6,
  },
  container: {
    // flex: .5,
    alignSelf: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    padding: 15,
  },
  cover: {
    borderRadius: 10,
    height: helpers.fullWidth * .55,
    overflow: 'hidden',
    // width: helpers.fullWidth * .35,
  },
  infoWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    width: helpers.fullWidth * .35 - 20,
  },
  price: {
    fontSize: 13,
  },
});
