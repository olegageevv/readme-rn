import { StyleSheet } from 'react-native';

import { colors, helpers } from 'src/styles';

export default StyleSheet.create({
  author: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  body: {
    margin: 15,
  },
  container: {
    backgroundColor: colors.white,
    borderRadius: 10,
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    marginHorizontal: 15,
    marginVertical: 5,
    position: 'relative',
    shadowColor: colors.navy,
    shadowOffset: {
      height: 8,
      width: 0,
    },
    shadowOpacity: 0.12,
    shadowRadius: 8,
    width: helpers.cardWidth,
  },
  footer: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    marginBottom: 15,
    paddingHorizontal: 15,
    width: helpers.cardWidth,
  },
  footerIcons: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
  },
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    marginHorizontal: 15,
    marginTop: 15,
    width: helpers.cardWidth - 30,
  },
  headerTime: {
    color: colors.gray,
    fontSize: 12,
  },
  highlight: {
    marginVertical: 15,
  },
  iconBlock: {
    alignItems: 'center',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    marginRight: 12,
  },
  iconText: {
    color: colors.blueDark,
    marginLeft: 6,
  },
  image: {
    borderRadius: 15,
    height: 30,
    marginRight: 10,
    resizeMode: 'cover',
    shadowColor: colors.navy,
    shadowOpacity: .05,
    width: 30,
  },
});
