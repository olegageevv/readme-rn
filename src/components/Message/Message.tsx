import moment from 'moment';
import React from 'react';
import { Image, Text, TouchableWithoutFeedback, View } from 'react-native';

import { Icon } from 'src/components';
import { IMessage } from 'src/models/IChannelState';
import { colors } from 'src/styles';

import styles from './Message.styles';

interface IProps {
  message: IMessage;
  highlight?: boolean;
  onPress?: () => void;
}

class Message extends React.Component<IProps> {
  render () {
    const {
      message,
      highlight,
      onPress,
    } = this.props;
    const { description, time, author, likes, comments } = message;
    const timeAgo = moment(time).toNow();
    const iconProps = {
      fill: colors.blueDark,
      height: 18,
      width: 18,
    };
    return (
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={[styles.container, highlight && styles.highlight]}>
          <View style={styles.header}>
            <View style={styles.author}>
              <Image
                style={styles.image}
                source={require('src/assets/img/no_profile.jpg')}
              />
              <View>
                <Text>{author}</Text>
                <Text style={styles.headerTime} numberOfLines={1}>{timeAgo}</Text>
              </View>
            </View>
            <Icon name="Dots" />
          </View>
          <View style={styles.body}>
            <Text>{description}</Text>
          </View>
          <View style={styles.footer}>
            <View style={styles.footerIcons}>
              <View style={styles.iconBlock}>
                <Icon name="Heart" {...iconProps} />
                {likes && <Text style={styles.iconText}>{likes}</Text>}
              </View>
              <View style={styles.iconBlock}>
                <Icon name="Comments" {...iconProps} />
                {comments && <Text style={styles.iconText}>{comments}</Text>}
              </View>
            </View>
            <View>
              <Icon name="Share" {...iconProps} />
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default Message;
