import React from 'react';
import {
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Icon, IMenuItem, Menu, Spacer, Title } from 'src/components';
import IRootState from 'src/models';
import { colors } from 'src/styles';

import styles from './Search.styles';

interface IProps {
  placeholder: string;
  onSubmitEditing: () => void;
}

interface IState {
  value: string;
  isFocused: boolean;
}

const suggestions: IMenuItem[] = [
  {
    id: '1',
    onPress: () => undefined,
    title: 'Читать бесплатно',
  },
  {
    id: '2',
    onPress: () => undefined,
    title: 'Наука мудрости',
  },
  {
    id: '3',
    onPress: () => undefined,
    title: 'Тонкое исскусство пофигизма',
  },
  {
    id: '4',
    onPress: () => undefined,
    title: 'Саморазвитие',
  },
  {
    id: '5',
    onPress: () => undefined,
    title: 'Биографии',
  },
  {
    id: '6',
    onPress: () => undefined,
    title: 'Нон-фикшн',
  },
  {
    id: '7',
    onPress: () => undefined,
    title: 'Нобелевская премия',
  },
  {
    id: '8',
    onPress: () => undefined,
    title: 'Семь историй успеха',
  },
  {
    id: '9',
    onPress: () => undefined,
    title: 'Бизнес',
  },
  {
    id: '10',
    onPress: () => undefined,
    title: 'Классика',
  },
  {
    id: '11',
    onPress: () => undefined,
    title: 'Антиутопии',
  },
];

class Search extends React.Component<IProps, IState> {
  static defaultProps = {
    onSubmitEditing: () => undefined,
    placeholder: 'Search',
  };

  input: TextInput | null = null;

  state = {
    isFocused: false,
    value: '',
  };

  renderResults = () => {
    const { isFocused } = this.state;
    if (!isFocused) return null;
    return (
      <View style={styles.resultContainer}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <Title>Suggestions</Title>
          <Menu items={suggestions} />
          <Spacer height={70} />
        </ScrollView>
      </View>
    );
  }

  render () {
    const {
      placeholder,
      onSubmitEditing,
    } = this.props;
    const {
      value,
      isFocused,
    } = this.state;
    const iconProps = { height: 16, width: 16 };
    return (
      <>
        <View style={styles.container}>
          <View style={[styles.input, isFocused && styles.inputFocused]}>
            <View style={styles.icon}>
              <Icon name="Search" {...iconProps} />
            </View>
            <TextInput
              ref={ref => (this.input = ref)}
              style={[styles.inputField, isFocused && styles.inputFieldFocused]}
              onChangeText={this.onChangeText}
              value={value}
              autoCorrect={false}
              autoCapitalize="none"
              returnKeyType="search"
              placeholder={placeholder}
              placeholderTextColor={colors.gray}
              onSubmitEditing={onSubmitEditing}
              onFocus={this.onFocus}
              onBlur={this.onBlur}
            />
            {
              value !== '' &&
              <TouchableOpacity style={styles.icon} onPress={this.clearInput}>
                <Icon name="Close" fill="gray" {...iconProps} />
              </TouchableOpacity>
            }
          </View>
          {
            isFocused &&
            <TouchableOpacity onPress={this.onCancel}>
              <View style={styles.cancelButton}>
                <Text style={styles.cancelButtonText} numberOfLines={1}>Cancel</Text>
              </View>
            </TouchableOpacity>
          }
        </View>
        {this.renderResults()}
      </>
    );
  }

  private clearInput = () => {
    this.onChangeText('');
  }

  private onFocus = () => {
    this.setState({ isFocused: true });
  }

  private onBlur = () => {
    if (this.input) this.input.blur();
  }

  private onChangeText = (value: string) => {
    this.setState({ value });
  };

  private onCancel = () => {
    if (this.input) this.input.blur();
    this.setState({ isFocused: false, value: '' });
  };
}

const mapStateToProps = (state: IRootState) => ({
  store: state.store,
});

const mapDispatchToProps = {};

export default withRouter(connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(Search));
