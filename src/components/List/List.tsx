import { push } from 'connected-react-router';
import React from 'react';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { Book, Books, HorizontalScroll, MenuItem, Message, Spacer, Title } from 'src/components';
import IRootState, { IBooksState, ILibraryState, IStoreState } from 'src/models';
import { IBook } from 'src/models/IBooksState';
import { IChannel, IMessage } from 'src/models/IChannelState';
import { ICollection } from 'src/models/ILibraryState';
import { ICategory } from 'src/models/IStoreState';

import Categories from './components/Categories';

import styles from './List.styles';

export type listType =
  'books'
  | 'library'
  | 'store'
  | 'messages'
  | 'events'
  ;

type IListItem = IBook
  | ICollection
  | ICategory
  | IMessage
  ;

interface IProps {
  type: listType;
  ids?: string[];
  channel?: IChannel;
  books: IBooksState;
  library: ILibraryState;
  store: IStoreState;
  bookOpen: (bookId: string) => void;
  categoryOpen: (categoryId: string) => void;
  channelOpen: (channelId: string) => void;
  collectionOpen: (collectionId: string) => void;
  numColumns?: number;
  title?: string;
  onPress?: () => void;
}

interface IState {
  items: IListItem[];
  loading: boolean;
  refreshing: boolean;
}

class List extends React.PureComponent<IProps, IState> {
  state = {
    items: [],
    loading: false,
    refreshing: false,
  };

  componentDidMount () {
    const {
      books,
      channel,
      library,
      store,
      type,
    } = this.props;

    switch (type) {
      case 'books':
        this.update(books);
        break;
      case 'messages':
        if (channel) this.update(channel);
        break;
      case 'library':
        this.update(library);
        break;
      case 'store':
        this.update(store);
        break;
      default:
    }
  }

  render () {
    const { numColumns } = this.props;
    const {
      items,
      loading,
      refreshing,
    } = this.state;
    return (
      <>
        <FlatList
          data={items}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          ItemSeparatorComponent={this.renderSeparator}
          // ListEmptyComponent={this.renderEmpty}
          ListFooterComponent={this.renderFooter}
          ListHeaderComponent={this.renderHeader}
          // onEndReached={this.handleLoadMore}
          // onEndReachedThreshold={0.01}
          // onRefresh={this.handleRefresh}
          refreshing={refreshing}
          extraData={loading}
          numColumns={numColumns}
          columnWrapperStyle={numColumns ? styles.column : undefined}
          keyboardShouldPersistTaps="handled"
        />
      </>
    );
  }

  private renderItem = (rowData: ListRenderItemInfo<any>) => {
    const {
      books,
      type,
      onPress,
    } = this.props;
    const onBookPress = () => this.props.bookOpen(rowData.item.id);
    const onCategoryPress = () => this.props.categoryOpen(rowData.item.id);
    const onCollectionPress = () => this.props.collectionOpen(rowData.item.id);
    const onMessagePress = () => this.props.channelOpen(rowData.item.id);
    switch (type) {
      case 'books':
        return <Book {...rowData.item} onPress={onBookPress} />;
      case 'library':
        return <MenuItem title={rowData.item.title} onPress={onCollectionPress} />;
      case 'messages':
        return <Message message={rowData.item} onPress={onPress || onMessagePress} />;
      case 'store':
        const data = rowData.item.books.map((bookId: string) => {
          return books.items[bookId];
        });
        return (
          <View>
            <Title onPress={onCategoryPress}>{rowData.item.title}</Title>
            <HorizontalScroll>
              <Books data={data} onPress={this.props.bookOpen} />
            </HorizontalScroll>
          </View>
        );
      default:
        return <View />;
    }
  };

  private renderSeparator = () => {
    const { type } = this.props;
    switch (type) {
      case 'library':
        return <View style={styles.separator} />;
      case 'messages':
        return <Spacer height={15} />;
      default:
        return null;
    }
  };

  private renderHeader = () => {
    const { type, ids, title } = this.props;
    const { items } = this.state;
    switch (type) {
      case 'library':
        const onPressLibrary = () => undefined;
        return title
          ? <Title onPress={onPressLibrary}>{title}</Title>
          : null;
      case 'messages':
        const onPressMessages = () => undefined;
        return title && items.length
          ? (
            <View style={styles.titleContainer}>
              <Title onPress={onPressMessages}>{title}</Title>
              <Spacer height={10} />
            </View>
          ) : null;
      case 'store':
        return !ids
          ? (
            <HorizontalScroll>
              <Categories />
            </HorizontalScroll>
          ) : null;
      default:
        return null;
    }
  };

  private renderFooter = () => {
    const { type } = this.props;
    switch (type) {
      case 'messages':
        return (
          <Spacer height={30} />
        );
      default:
        return null;
    }
  };

  private update = (data: any | IBooksState | ILibraryState | IStoreState | IChannel) => {
    const {
      ids,
      type,
    } = this.props;
    let items = (ids || Object.keys(data.items)).map(id => data.items[id]);
    if (!ids && type === 'store') {
      items = items.filter(item => !item.categories && item.featured);
    }
    this.setState({
      items,
      loading: data.loading,
      refreshing: data.refreshing,
    });
  };

  private keyExtractor = (item: IListItem) => item.id;
}

const mapStateToProps = (state: IRootState) => ({
  books: state.books,
  library: state.library,
  store: state.store,
});

const mapDispatchToProps = {
  bookOpen: (bookId: string) => push(`/book/${bookId}`),
  categoryOpen: (categoryId: string) => push(`/store/${categoryId}/books`),
  channelOpen: (channelId: string) => push(`/channel/${channelId}`),
  collectionOpen: (collectionId: string) => push(`/library/${collectionId}`),
};

export default withRouter(connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(List));
