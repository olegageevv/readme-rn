import { push } from 'connected-react-router';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import IRootState, { IStoreState } from 'src/models';
import { ICategory } from 'src/models/IStoreState';

import CategoryItem from './components/CategoryItem';

interface IProps {
  store: IStoreState;
  categoryOpen: (categoryId: string) => void;
}

export class Categories extends React.Component<IProps> {
  render () {
    const { store } = this.props;
    // featured
    const categories: ICategory[] = Object.keys(store.items).map(id => store.items[id]);
    const featuredCategories = categories.filter(c => c.featured && c.categories);
    return featuredCategories.map((category) => {
      const onPress = () => this.props.categoryOpen(category.id);
      return (
        <CategoryItem
          key={category.id}
          {...category}
          onPress={onPress}
        />
      );
    });
  }
}

const mapStateToProps = (state: IRootState) => ({
  store: state.store,
});

const mapDispatchToProps = {
  categoryOpen: (categoryId: string) => push(`/store/${categoryId}/categories`),
};

export default withRouter(connect<any, any, any, IRootState>(
  mapStateToProps,
  mapDispatchToProps,
)(Categories));
