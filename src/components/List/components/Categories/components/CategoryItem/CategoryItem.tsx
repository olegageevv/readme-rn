import React from 'react';
import { View } from 'react-native';

import { Button } from 'src/components';

import styles from './CategoryItem.styles';

interface IProps {
  title: string;
  onPress: () => void;
}

export class CategoryItem extends React.PureComponent<IProps> {
  render () {
    const {
      title,
      onPress,
    } = this.props;
    return (
      <View style={styles.container}>
        <Button
          label={title}
          onPress={onPress}
          bordered={true}
          color="navy"
          size="small"
        />
      </View>
    );
  }
}

export default CategoryItem;
