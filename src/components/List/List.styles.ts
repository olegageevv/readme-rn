import { StyleSheet } from 'react-native';

import { colors } from 'src/styles';

export default StyleSheet.create({
  column: {
    justifyContent: 'space-evenly',
  },
  container: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginBottom: 50,
  },
  separator: {
    borderBottomColor: colors.grayLight,
    borderBottomWidth: 1,
    marginHorizontal: 30,
  },
  titleContainer: {
    paddingHorizontal: 15,
    paddingTop: 15,
  },
});
