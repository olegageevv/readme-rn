import { StyleSheet } from 'react-native';

import { colors } from 'src/styles';

export default StyleSheet.create({
  container: {},
  separator: {
    borderBottomColor: colors.grayLight,
    borderBottomWidth: 1,
    marginHorizontal: 30,
  },
});
