import { StyleSheet } from 'react-native';

import { base, colors, fonts } from 'src/styles';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 48,
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  icon: {
    marginRight: 10,
  },
  text: {
    fontFamily: fonts.fontRegular,
    fontSize: 14,
  },
});
