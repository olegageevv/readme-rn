import React from 'react';
import { Text, TouchableWithoutFeedback, View } from 'react-native';

import { Icon, IMenuItem } from 'src/components';
import { colors } from 'src/styles';
import styles from './MenuItem.styles';

export class MenuItem extends React.PureComponent<IMenuItem> {
  static defaultProps = {
    color: 'navy',
    id: '',
    onPress: () => undefined,
    title: '',
  };

  render () {
    const  { title, onPress, color, icon } = this.props;
    return (
      <TouchableWithoutFeedback onPress={onPress}>
        <View style={styles.container}>
          {
            icon &&
            <View style={styles.icon}>
              <Icon name={icon} fill={colors.gray} width={12} height={12} />
            </View>
          }
          <Text style={[styles.text, color && { color: colors[color] }]}>{title}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default MenuItem;
