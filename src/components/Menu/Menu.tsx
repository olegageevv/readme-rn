import React from 'react';
import { FlatList, ListRenderItemInfo, View } from 'react-native';

import { MenuItem } from 'src/components';

import styles from './Menu.styles';

export interface IMenuItem {
  id: string;
  onPress: () => void;
  color?: 'red' | 'navy';
  title: string;
  icon?: string;
}

interface IProps {
  items: IMenuItem[];
}

export class Menu extends React.PureComponent<IProps> {

  render () {
    const { items } = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          ItemSeparatorComponent={this.renderSeparator}
          // ListEmptyComponent={this.renderEmpty}
          // ListFooterComponent={this.renderFooter}
          // ListHeaderComponent={this.renderHeader}
          // onEndReached={this.handleLoadMore}
          // onEndReachedThreshold={0.01}
          // onRefresh={this.handleRefresh}
          // refreshing={refreshing}
          // extraData={loading}
          // numColumns={numColumns}
        />
      </View>
    );
  }

  private renderItem = (rowData: ListRenderItemInfo<IMenuItem>) => <MenuItem {...rowData.item} />;

  private renderSeparator = () => <View style={styles.separator} />;

  private keyExtractor = (item: IMenuItem) => item.id;
}

export default Menu;
