import * as React from 'react';
// Note: test renderer must be required after react-native.
import { StyleSheet, TouchableWithoutFeedback, View } from 'react-native';
import renderer from 'react-test-renderer';

import { MenuItem } from 'src/components';
import { colors } from 'src/styles';

it('should render nothing', () => {
  const root = renderer.create(
    <View />,
  ).root;
  const viewInstance = root.findByType('View');
  expect(viewInstance.children).toHaveLength(0);
});

it('should render items', () => {
  const item1 = { id: '1', title: 'text1' };
  const item2 = { id: '2', title: 'text2' };
  const root = renderer.create(
    <View>
      <MenuItem {...item1} />
      <MenuItem {...item2} />
    </View>,
  ).root;
  const items = root.findAllByType(MenuItem);
  expect(items).toHaveLength(2);
});

it('should call onPress callback', () => {
  const onPressCallback = jest.fn();
  const root = renderer.create(
    <View>
      <MenuItem onPress={onPressCallback} />
    </View>,
  ).root;
  root.findByType(TouchableWithoutFeedback).props.onPress();
  expect(onPressCallback).toBeCalled();
});

it('should set color for item', () => {
  const root = renderer.create(
    <View>
      <MenuItem color="red" />
    </View>,
  ).root;
  const menuItem = root.findByType(MenuItem);
  const styles = StyleSheet.flatten(
    menuItem.findByType('Text').props.style,
  );
  expect(styles).toHaveProperty('color', (colors as any)[menuItem.props.color]);
});
