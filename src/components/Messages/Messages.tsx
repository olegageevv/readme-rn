import React from 'react';
import { View } from 'react-native';

import { Message } from 'src/components';
import { IMessage } from 'src/models/IChannelState';

import styles from './Messages.styles';

interface IProps {
  data: IMessage[];
  onPress: (messageId: string) => void;
}

class Messages extends React.PureComponent<IProps> {
  render () {
    const {
      data,
    } = this.props;
    return data.map((item) => {
      const onPress = () => this.props.onPress(item.id);
      return (
        <View key={item.id} style={styles.container}>
          <Message message={item} onPress={onPress} />
        </View>
      );
    });
  }
}

export default Messages;
