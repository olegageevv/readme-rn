import React from 'react';
import { G, Polygon } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="check" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Polygon points="7 15.448 1.75 10.119 0 11.896 7 19 22 3.776 20.25 2" />
    </G>
  ),
  viewBox: '0 0 22 22',
};
