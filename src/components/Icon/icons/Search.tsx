import React from 'react';
import { G, Path } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="search" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Path d="M9.001,15 C5.692,15 3,12.308 3,9 C3,5.691 5.692,3 9.001,3 C12.309,3 15,5.691 15,9 C15,12.308 12.309,15 9.001,15 Z M21.728,20.292 L15.312,13.904 C16.366,12.549 17,10.85 17,9 C17,4.583 13.418,1 9.001,1 C4.583,1 1,4.583 1,9 C1,13.418 4.583,17 9.001,17 C10.847,17 12.543,16.368 13.897,15.317 L20.316,21.708 L21.728,20.292 Z" />
    </G>
  ),
  viewBox: '0 0 22 22',
};
