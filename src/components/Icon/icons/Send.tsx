import React from 'react';
import { G, Path } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="send" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Path d="M21.8560833,1 L19.9668333,1.62975 L19.9485,1.61095833 L19.911375,1.64808333 L0,8.28520833 L10.0874583,12.768625 L14.570875,22.8560833 L21.208,2.94425 L21.2446667,2.90758333 L21.2263333,2.88925 L21.8560833,1 Z M5.06366667,8.52995833 L17.012875,4.54658333 L10.5787917,10.981125 L5.06366667,8.52995833 Z M14.326125,17.7924167 L11.8749583,12.2772917 L18.3090417,5.84320833 L14.326125,17.7924167 Z" fill-rule="nonzero" />
    </G>
  ),
  viewBox: '0 0 22 22',
};
