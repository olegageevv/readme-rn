import React from 'react';
import { G, Path } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="close" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Path d="M12.4142136,11 L18.7781746,17.363961 L17.363961,18.7781746 L11,12.4142136 L4.63603897,18.7781746 L3.22182541,17.363961 L9.58578644,11 L3.22182541,4.63603897 L4.63603897,3.22182541 L11,9.58578644 L17.363961,3.22182541 L18.7781746,4.63603897 L12.4142136,11 Z" fill-rule="nonzero" />
    </G>
  ),
  viewBox: '0 0 22 22',
};
