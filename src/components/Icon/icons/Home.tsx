import React from 'react';
import { G, Path } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="home" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Path
        d="M18,10.1259107 L11.9832931,4.69054675 L6,10.0444565 L6,21 L18,21 L18,10.1259107 Z M20,11.9326678 L20,23 L4,23 L4,11.8340762 L2.66682508,13.0270143 L1.33317492,11.5365857 L11.989665,2.00105325 L22.6703487,11.6497537 L21.3296513,13.1338463 L20,11.9326678 Z"
      />
    </G>
  ),
  viewBox: '0 0 22 22',
};
