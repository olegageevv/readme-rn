import React from 'react';
import { G, Path } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="plus" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Path d="M12,12 L12,21 L10,21 L10,12 L1,12 L1,10 L10,10 L10,1 L12,1 L12,10 L21,10 L21,12 L12,12 Z" fill-rule="nonzero" />
    </G>
  ),
  viewBox: '0 0 22 22',
};
