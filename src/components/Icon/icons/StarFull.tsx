import React from 'react';
import { G, Path } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="star-full" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Path d="M11.0003479,0.999800492 L14.3994434,7.91274855 L21.9997018,9.02101483 L16.5005218,14.4017424 L17.798539,22 L11.0003479,18.4128483 L4.20116285,22 L5.50017393,14.4017424 L0,9.02101483 L7.60025841,7.91274855 L11.0003479,0.999800492 Z" fill-rule="nonzero" />
    </G>
  ),
  viewBox: '0 0 22 22',
};
