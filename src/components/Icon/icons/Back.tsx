import React from 'react';
import { G, Path } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="back" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Path d="M3.84128286,10 L22.0238248,10 L22.0238248,12 L3.88097943,12 L10.1342829,18.1892631 L8.72736675,19.6107367 L0,10.9727472 L8.72961924,2.38704077 L10.1320304,3.81295904 L3.84128286,10 Z" fill-rule="nonzero" />
    </G>
  ),
  viewBox: '0 0 22 22',
};
