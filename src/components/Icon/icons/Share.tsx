import React from 'react';
import { G, Path } from 'react-native-svg';

// tslint:disable:max-line-length
export default {
  svg: (
    <G id="share" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <Path d="M13.1377,12.562 L14.1377,12.562 L14.1377,16 L18.8087,10.662 L14.1377,5.323 L14.1377,8.647 L13.1527,8.662 C7.5357,8.744 4.1137,12.155 2.7997,15.986 C5.8577,13.737 9.3827,12.562 13.1377,12.562 Z M12.1377,21.323 L12.1377,14.592 C8.2957,14.828 4.8657,16.432 1.9287,19.369 L-0.0003,21.297 L0.2247,18.579 C0.6877,12.993 4.9097,7.286 12.1377,6.708 L12.1377,0 L21.4667,10.662 L12.1377,21.323 Z" fill-rule="nonzero" />
    </G>
  ),
  viewBox: '0 0 22 22',
};
