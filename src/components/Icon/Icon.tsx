import React from 'react';
import SvgIcon from 'react-native-svg-icon';
import * as icons from './icons';

import { colors } from 'src/styles';

interface IProps {
  name: string;
  fill: string;
  height: number;
  viewBox: string;
  width: number;
}

class Icon extends React.PureComponent<IProps> {
  static defaultProps = {
    fill: colors.navy,
    height: 22,
    viewBox: '0 0 22 22',
    width: 22,
  };

  render () {
    return <SvgIcon {...this.props} svgs={icons} />;
  }
}

export default Icon;
