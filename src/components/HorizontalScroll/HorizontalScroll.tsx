import React, { ReactElement } from 'react';
import { Animated, Platform, ViewPagerAndroid } from 'react-native';

const AnimatedViewPagerAndroid = Platform.OS === 'android' ?
  Animated.createAnimatedComponent(ViewPagerAndroid) :
  undefined;

interface IProps {
  children: ReactElement<any>;
}

class HorizontalScroll extends React.PureComponent<IProps> {
  offset = new Animated.Value(0);

  render () {
    const {
      children,
    } = this.props;
    if (Platform.OS === 'ios') {
      return (
        <Animated.ScrollView
          horizontal={true}
          automaticallyAdjustContentInsets={false}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: this.offset } } }],
            { useNativeDriver: true },
          )}
          scrollEventThrottle={16}
          scrollsToTop={false}
          showsHorizontalScrollIndicator={false}
          directionalLockEnabled={true}
          alwaysBounceVertical={false}
          keyboardDismissMode="on-drag"
        >
          {children}
        </Animated.ScrollView>
      );
    }
    return (
      <AnimatedViewPagerAndroid
        style={{ flex: 1 }}
        keyboardDismissMode="on-drag"
        onPageScroll={Animated.event(
          [{ nativeEvent: { offset: this.offset } }],
          { useNativeDriver: true },
        )}
      >
        {children}
      </AnimatedViewPagerAndroid>
    );
  }
}

export default HorizontalScroll;
