import React from 'react';
import { View } from 'react-native';
import Spinner from 'react-native-spinkit';

import { colors } from 'src/styles';
import styles from './Loading.styles';

export type SpinnerColor =
  'blue'
  | 'red'
  | 'green'
  | 'gray'
  | 'gold'
  | 'navy';

export interface IProps {
  fill?: SpinnerColor;
  fullScreen?: boolean;
  size?: number;
}

class Loading extends React.PureComponent<IProps> {
  getColor (color: string): string {
    return (colors as any)[color];
  }

  render () {
    const { fullScreen = false, size = 50, fill } = this.props;
    const color = fill ? this.getColor(fill) : colors.gold;
    return (
      <View style={fullScreen ? styles.containerFull : styles.container}>
        <Spinner size={size} type="Pulse" color={color} />
      </View>
    );
  }
}

export default Loading;
