import 'jest';
import * as React from 'react';
import 'react-native';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import ShallowRenderer from 'react-test-renderer/shallow';

import Loading from '../Loading';
import styles from '../Loading.styles';

it('renders correctly', () => {
  const tree = renderer.create(
    <Loading fullScreen={true} size={60} />,
  ).toJSON();
  expect(tree)
    .toMatchSnapshot();
});

it('should render fullscreen and custom size', () => {
  const props = { fullScreen: true, size: 60 };
  const shallow = ShallowRenderer.createRenderer();
  shallow.render(<Loading {...props} />);
  const tree = shallow.getRenderOutput();
  expect(tree.props)
    .toHaveProperty('style', styles.containerFull);
  expect(tree.props)
    .toHaveProperty('children.props.size', props.size);
});
