import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    alignSelf: 'center',
    marginBottom: 10,
    marginTop: 10,
  },
  containerFull: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});
