import { RouterState } from 'connected-react-router';

import { ICoreState } from 'src/models';
import routes, { IMenuItem } from 'src/routes';

import { getActiveNavMenuItemKey } from '..';

const getRouterState = (newPathName: string): RouterState => ({
  action: 'PUSH',
  location: {
    hash: '1234',
    pathname: newPathName,
    search: '',
    state: null,
  },
});

describe('getActiveNavMenuItemKey', () => {
  const coreState: ICoreState = {
    activeMenuKey: '/',
    intro: false,
    loading: false,
  };

  it('should get active menu item key', () => {
    expect(getActiveNavMenuItemKey({
      core: coreState,
      router: getRouterState('/'),
    })).toBe('home');

    expect(getActiveNavMenuItemKey({
      core: coreState,
      router: getRouterState('/profile'),
    })).toBe('profile');

    expect(getActiveNavMenuItemKey({
      core: coreState,
      router: getRouterState('/profile/submodule'),
    })).toBe('profile');
  });

});
