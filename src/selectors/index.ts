import IRootState from 'src/models';
import routes from 'src/routes';

/**
 * Get key of active menu item.
 *
 * @param {IRootStateWithRouter} state Global state with router state.
 * @returns {string} Key of menu item.
 */
export const getActiveNavMenuItemKey = (state: Partial<IRootState>): string => {
  const currentPath = state.router!.location.pathname || '';
  let activeMenuKey = '';
  routes.forEach((item) => {
    if (currentPath.startsWith(item.link)) {
      activeMenuKey = item.id;
    }
  });
  return activeMenuKey;
};
