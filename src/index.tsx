import { ConnectedRouter } from 'connected-react-router';
import React from 'react';
import { SafeAreaView, YellowBox } from 'react-native';
import { Provider } from 'react-redux';
import { NativeRouter, Route, Switch } from 'react-router-native';

import { PersistGate } from 'redux-persist/integration/react';

YellowBox.ignoreWarnings(['Require cycle:', 'unknown call: "relay:check"']);

import { Loading } from 'src/components';
import { AuthScreen, BookScreen, ChannelScreen, IntroScreen, NavScreen } from 'src/screens';
import configureStore from 'src/store';
import { base } from 'src/styles';

const { persistor, history, store } = configureStore();

class Root extends React.Component {
  render () {
    const loading = <Loading fullScreen={true} />;
    return (
      <NativeRouter>
        <Provider store={store}>
          <PersistGate loading={loading} persistor={persistor}>
            <ConnectedRouter history={history}>
              <SafeAreaView style={base.baseContainer}>
                <Switch>
                  <Route path="/book/:bookId" component={BookScreen} />
                  <Route path="/channel/:channelId" component={ChannelScreen} />
                  <Route path="/auth" component={AuthScreen} />
                  <Route path="/intro" component={IntroScreen} />
                  <Route path="/" component={NavScreen} />
                </Switch>
              </SafeAreaView>
            </ConnectedRouter>
          </PersistGate>
        </Provider>
      </NativeRouter>
    );
  }
}

export default Root;
