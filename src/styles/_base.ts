import { StyleSheet } from 'react-native';

import colors from './_colors';

export default StyleSheet.create({
  baseContainer: {
    alignItems: 'stretch',
    flex: 1,
    justifyContent: 'center',
  },
  container: {
    backgroundColor: colors.grayLighter,
    flex: 1,
    justifyContent: 'flex-start',
  },
  contentPaddingSmall: {
    paddingLeft: 16,
    paddingRight: 16,
  },
});
