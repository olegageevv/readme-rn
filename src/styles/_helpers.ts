import { Dimensions, Platform, PlatformIOS } from 'react-native';

const { height, width } = Dimensions.get('window');

const setTransparency = (color: string, alpha: number) => {
  const hexAlpha = (alpha + 0x10000).toString(16).substr(-2);
  return `${color}${hexAlpha}`;
};

const isIphoneX = (): boolean => {
  const dimen = Dimensions.get('window');
  return (
    Platform.OS === 'ios' &&
    !PlatformIOS.isPad &&
    !PlatformIOS.isTVOS &&
    ((dimen.height === 812 || dimen.width === 812) || (dimen.height === 896 || dimen.width === 896))
  );
};

export default {
  isIphoneX,
  setTransparency,
  cardWidth: width - 60,
  fullHeight: height,
  fullWidth: width,
};
