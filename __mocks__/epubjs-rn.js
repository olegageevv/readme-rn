const existsMock = jest.fn();
existsMock.mockReturnValueOnce({then: jest.fn()});

export default {
  Epub: () => {},
  Rendition: () => {},
  Streamer: () => {},
};
